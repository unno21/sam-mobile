package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.APIResponse;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Item;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Review;
import com.example.papsirehv.sam_v11.models.SubCategory;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.models.Wishlist;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GarmentDetailsPresenter {
    public interface View {
        void showProductDetails(Product product);
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void changeHeartStatus(Boolean inWishList);
        void changeImage(SubCategory subCategory);
        void hideColorButtons(int[] index);
    }

    private View view;
    private Cart cart;
    private SubCategory subCategory;
    public GarmentDetailsPresenter(View view) { this.view = view; }

    public void showProductDetails() {
        Product product = Global.SelectedProduct;
        hideColors(product);
        subCategory = product.getSubCategories().get(0);
        view.showProductDetails(product);
    }
    public void addToCart() {
        Item item = new Item(subCategory, 1);
        checkCart();
        checkCart();
        Cart cart = Global.Cart;
        String url = Global.API_URL + "cart/add";
        cart.addItem(
                item,
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Item addItem = gson.fromJson(result, Item.class);

                        if (addItem.getId() != null) {
                            view.showSuccessAlert("Item has been added to your cart.");
                        } else {
                            view.showErrorAlert("An error occured.");
                        }

                    } catch (Exception ex) {
                        view.showErrorAlert("Not enough stock.");
                    }
                },
                (String error) -> {
                    //view.showErrorAlert(error);
                });
    }
    public void addToWish(Product product) {
        User user = Global.User;
        String url = Global.API_URL + "wishlist/add";

        Wishlist.addItem(
                product,
                user,
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Wishlist addWish = gson.fromJson(result, Wishlist.class);

                        if (addWish.getId() != null) {
                            view.showSuccessAlert("Item has been added to your wishlist.");
                            getWishlist();
                        } else {
                            view.showErrorAlert("Item is already in the wish list.");
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> {
                    //view.showErrorAlert(error);
                });
    }

    public void removeFromWish(Product product) {

        String url = Global.API_URL + "wishlist/remove/" + product.getId() + "/" + Global.User.getId();
        Wishlist.removeItem(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        APIResponse response = gson.fromJson(result, APIResponse.class);

                        if (response.getResult().equals("success")) {
                            view.showSuccessAlert("Item has been removed from your wishlist.");
                            getWishlist();

                        } else {
                            view.showErrorAlert(response.getMessage());
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> {

                });

    }

    private void checkCart() {
        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        // create copy for presenter
                        this.cart = cart;
                        Global.Cart = cart;


                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());

                    }
                },
                (String error) -> view.showErrorAlert("")
        );

    }

    public ArrayList<Review> getProductReviews()
    {
        return Global.SelectedProduct.getReviews();
    }

    public void checkWishList()
    {
        Product product = Global.SelectedProduct;
        ArrayList<Wishlist> wishlists = Global.Wishlists;
        if (Global.Wishlists != null) {
            for (Wishlist wishlist : wishlists) {
                if (wishlist.getProduct().getId().equals(product.getId())) {
                    this.view.changeHeartStatus(true);
                    break;
                }
                this.view.changeHeartStatus(false);
            }

        } else {
            getWishlist();
        }
    }

    public void getWishlist() {
        String url = Global.API_URL + "wishlist/" + Global.User.getId();
        Wishlist.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Wishlist[] wishlist = gson.fromJson(result, Wishlist[].class);

                        Global.Wishlists = new ArrayList<Wishlist>(Arrays.asList(wishlist));
                        this.checkWishList();
                    } catch (Exception ex) {
                        view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void changeColor(String color)
    {
        Product product = Global.SelectedProduct;
        for (SubCategory subCategory : product.getSubCategories()) {
            if (subCategory.getCategory().getName().equals(color.toLowerCase())) {
                this.subCategory = subCategory;
                view.changeImage(subCategory);
                return;
            }
        }
        view.showErrorAlert("Sorry, no stocks available for " + color + " color.");

    }

    //hide color buttons
    private void hideColors(Product product) {

        ArrayList<String> strIndexes = new ArrayList<>();
        List<String> colors = new ArrayList<>( Arrays.asList("yellow", "blue", "pink", "red", "orange", "purple", "brown", "green", "others"));
        ArrayList<SubCategory> subCategories = product.getSubCategories();

        //copy category colors to list to compare to color list
        ArrayList<String> subCategoryColors = new ArrayList<>();
        for (int i = 0; i < subCategories.size(); i++) {
            subCategoryColors.add(subCategories.get(i).getCategory().getName());
        }

        for (int i = 0; i < colors.size(); i++) {
            if (subCategoryColors.indexOf(colors.get(i)) == -1)
                strIndexes.add(String.valueOf(i));
        }

        //convert arraylist to array
        int[] indexes = new int[strIndexes.size()];
        for (int i = 0; i < strIndexes.size(); i++){
            indexes[i] = Integer.parseInt(strIndexes.get(i));
        }

        view.hideColorButtons(indexes);
    }
}
