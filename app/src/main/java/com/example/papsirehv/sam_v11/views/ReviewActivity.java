package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.BuildConfig;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.OrderDetail;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.ProductReview;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;

public class ReviewActivity extends AppCompatActivity {

    ProductReview review;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Write Review");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setNavigationOnClickListener(view -> finish());

        ImageView imgReviewGarment = findViewById(R.id.imgGarmentPic);
        TextView lblReviewProductName = findViewById(R.id.lblProductName);
        TextView lblReviewProductCategory = findViewById(R.id.lblCategory);

        EditText txtReview = findViewById(R.id.txtReview);
        RatingBar userReviewRatingBar = findViewById(R.id.userReviewRatingBar);
        Button btnSubmitReview = findViewById(R.id.btnSubmitReview);

        ArrayList<Button> btnTemplates = new ArrayList<>();
        btnTemplates.add(findViewById(R.id.btnTemplate1));
        btnTemplates.add(findViewById(R.id.btnTemplate2));
        btnTemplates.add(findViewById(R.id.btnTemplate3));
        btnTemplates.add(findViewById(R.id.btnTemplate4));

        for (Button b : btnTemplates) {
            b.setOnClickListener(v -> {
                Button eventSender = (Button)v;
                txtReview.setText(txtReview.getText().toString() + eventSender.getText().toString() + ". ");
            });
        }


        OrderDetail orderDetail = Global.SelectedOrderDetail;
        String imagePath = Global.STORAGE_PATH + "products/" + orderDetail.getSub_category().getImage();
        Picasso.get().load(imagePath).into(imgReviewGarment);
        lblReviewProductName.setText(orderDetail.getProduct().getName());
        lblReviewProductCategory.setText(orderDetail.getProduct().getCategory().getName());

        User user = Global.User;
        Product product = Global.SelectedOrderDetail.getProduct();

        String url = Global.API_URL + "product/review/find/" + product.getId() + "/" + user.getId();
        ProductReview.get(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        review = gson.fromJson(result, ProductReview.class);

                        txtReview.setText(review.getComment());
                        userReviewRatingBar.setRating(review.getRate());

                    } catch (Exception ex) {
                        userReviewRatingBar.setRating(5);
                    }
                },
                (String error) -> {}
        );

        btnSubmitReview.setOnClickListener(v -> {

            if (review == null) {
                review = new ProductReview();
                review.setUser_id(user.getId());
                review.setProduct_id(product.getId());
            }

            float rate = userReviewRatingBar.getRating();
            String comment = txtReview.getText().toString();

            review.setRate(rate);
            review.setComment(comment);

            String url_add =  Global.API_URL + "product/review/add";

            review.save(
                    url_add,
                    (String result) -> {
                        Gson gson = new Gson();
                        try {
                            review = gson.fromJson(result, ProductReview.class);

                            Toast.makeText(ReviewActivity.this, "Product review saved.", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(ReviewActivity.this, OrderDetailsActivity.class));
                            finish();

                        } catch (Exception ex) {

                        }
                    },
                    (String error) -> {}
            );

        });
    }
}
