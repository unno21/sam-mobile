package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class Wishlist {
    private String id;
    private String user_id;
    private Product product;

    public Wishlist() {}
    public Wishlist(String user_id) { this.user_id = user_id; }

    public Product getProduct() { return this.product; }
    public String getId() { return this.id; }
    public void setUser_id(String user_id) { this.user_id = user_id; }
    public String getUser_id() { return this.user_id; }

    private void execute(String url, Callback successCallback, Callback errorCallback) {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("user_id", this.user_id));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();


    }
    public void save(String url, Callback successCallback) {
        this.execute(url, successCallback, (String error) -> {});
    }
    public void save(String url, Callback successCallback, Callback errorCallback) {
        this.execute(url, successCallback, errorCallback);
    }

    public static void getFromAPI(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public static void addItem(Product product, User user, String url, Callback successCallback, Callback errorCallback) {
         /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", product.getId()));
        data.add(new KeyValuePair("user_id", user.getId()));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public static void removeItem(String url, Callback successCallback, Callback errorCallback) {
        Cart.getFromAPI(url, successCallback, errorCallback);
    }
}
