package com.example.papsirehv.sam_v11.models;

public class APIResponse {
    private String result;
    private String message;
    public APIResponse(){}
    public String getResult() { return this.result; }
    public String getMessage() { return this.message; }
}
