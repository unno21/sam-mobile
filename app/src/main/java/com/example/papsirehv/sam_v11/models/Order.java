package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;

import java.util.ArrayList;

public class Order {
    private String id;
    private OrderDetail[] order_details;
    private String order_number;
    private String phone_number;
    private String address;
    private String order_status;
    private String type;
    private int total;
    private String date;

    public String getId() {
        return id;
    }

    public OrderDetail[] getOrder_details() {
        return order_details;
    }

    public String getOrder_number() {
        return order_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getAddress() {
        return address;
    }

    public String getOrder_status() {
        return order_status;
    }

    public String getType() {
        return type;
    }

    public int getTotal() {
        return total;
    }

    public String getDate() {
        return this.date;
    }

    public static void getAll(String url, Callback successCallback, Callback errorCallback)
    {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public void cancel(String url, Callback successCallback, Callback errorCallback)
    {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
}
