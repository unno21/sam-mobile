package com.example.papsirehv.sam_v11.cart_recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Item;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Size;
import com.example.papsirehv.sam_v11.models.SubCategory;
import com.example.papsirehv.sam_v11.presenters.CartPresenter;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sonu on 24/07/17.
 */

public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemRecyclerViewAdapter.ItemViewHolder> {

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView txtCartProductName, txtCartCategory, txtCartProductPrice;
        private ImageView imgCartProductImage;
        private Button btnRemoveFromCart;
        private Spinner spnrSize, spnrQuantity, spnrColor;

        public ItemViewHolder(View itemView) {
            super(itemView);
            txtCartProductName = itemView.findViewById(R.id.txtCartProductName);
            txtCartCategory = itemView.findViewById(R.id.txtCartCategory);
            txtCartProductPrice = itemView.findViewById(R.id.txtCartProductPrice);
            imgCartProductImage = itemView.findViewById(R.id.imgCartProductImage);
            spnrSize = itemView.findViewById(R.id.spnrSize);
            spnrQuantity = itemView.findViewById(R.id.spnrQuantity);
            spnrColor = itemView.findViewById(R.id.spnrColor);

            btnRemoveFromCart = itemView.findViewById(R.id.btnRemoveFromCart);
        }
    }

    private Context context;
    private ArrayList<Item> items;
    private String image_path;

    public ItemRecyclerViewAdapter(Context context, ArrayList<Item> items, String image_path) {
        this.context = context;
        this.items = items;
        this.image_path = image_path;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cart_row, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Picasso.get().load(image_path +  items.get(position).product_filters.getImage()).into(holder.imgCartProductImage);
        holder.txtCartProductName.setText(items.get(position).getProduct().getName());
        holder.txtCartCategory.setText(items.get(position).getProduct().getCategory().getName());
        holder.txtCartProductPrice.setText("₱ " + items.get(position).getProduct().getPrice() + "0");
//        holder.spnrSize.setSelection(2);
        holder.spnrQuantity.setSelection(items.get(position).getQuantity() - 1);
        holder.btnRemoveFromCart.setOnClickListener(v -> {
            CartPresenter mCartPresenter = new CartPresenter((CartPresenter.View)this.context);
            mCartPresenter.removeFromCart(items.get(position));
        });

        /*
            COLOR SPINNER
         */
        holder.spnrColor.setAdapter(null);
        List<String> spinnerColorArray =  new ArrayList<String>();
        int colorSelectedIndex = 0;
        int counter = 0;

        if (this.items.get(position).getProduct().getSubCategories().size() > 0) {
            for(SubCategory s : this.items.get(position).getProduct().getSubCategories()) {
                spinnerColorArray.add(s.getCategory().getName());
                try {
                    if (this.items.get(position).getSub_category_id().equals(s.getID())) {
                        colorSelectedIndex = counter;
                    }
                } catch (Exception ex) { }

                counter++;
            }
        } else {
            spinnerColorArray.add("Default");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_item, spinnerColorArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        holder.spnrColor.setAdapter(adapter);
        holder.spnrColor.setSelection(colorSelectedIndex);

        /*
            Quantity SPINNER
         */
        holder.spnrQuantity.setAdapter(null);
        List<String> spinnerQuantityArray =  new ArrayList<String>();
        int quantitySelectedIndex = 0;

        for(int i = 0; i < this.items.get(position).getSubcategory().getStock() +  this.items.get(position).getQuantity(); i++) {
            spinnerQuantityArray.add(String.valueOf(i + 1));
        }
        ArrayAdapter<String> quantityAdapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_item, spinnerQuantityArray);
        quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        holder.spnrQuantity.setAdapter(quantityAdapter);
        holder.spnrQuantity.setSelection(items.get(position).getQuantity() - 1);

        /*
            SIZE SPINNER
         */
        holder.spnrSize.setAdapter(null);
        List<String> spinnerSizeArray =  new ArrayList<String>();
        int sizeSelectedIndex = 0;
        counter = 0;

        for(String s : this.items.get(position).getProduct().getSizes()) {
            spinnerSizeArray.add(s);
            if (this.items.get(position).getSize().equals(s)) {
                sizeSelectedIndex = counter;
            }
            counter++;
        }

        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_item, spinnerSizeArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        holder.spnrSize.setAdapter(sizeAdapter);
        holder.spnrSize.setSelection(sizeSelectedIndex);



        /*
            Spinner Events
         */

        holder.spnrQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Item item = items.get(position);
                item.setQuantity(pos + 1);
                CartPresenter mCartPresenter = new CartPresenter((CartPresenter.View)context);
                mCartPresenter.updateItem(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spnrColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Item item = items.get(position);
                Product product = item.getProduct();
                if (product.getSubCategories().size() > 0) {
                    item.setColor(product.getSubCategories().get(pos).getID());
                    CartPresenter mCartPresenter = new CartPresenter((CartPresenter.View)context);
                    mCartPresenter.updateItem(item);
                    Picasso.get().load(image_path + product.getSubCategories().get(pos).getImage()).into(holder.imgCartProductImage);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.spnrSize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                Item item = items.get(position);
                Product product = item.getProduct();
                item.setSize(product.getSizes()[pos]);
                CartPresenter mCartPresenter = new CartPresenter((CartPresenter.View)context);
                mCartPresenter.updateItem(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
