package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class Cart {
    private String id;
    private String user_id;
    private ArrayList<Item> items;

    public Cart() {}
    public Cart(String user_id) { this.user_id = user_id; }

    public ArrayList<Item> getItems() { return items; }
    public void addItem(Item item) { this.items.add(item); }
    public void removeItem(Item item) { this.items.remove(item); }
    public void setUser_id(String user_id) { this.user_id = user_id; }
    public String getUser_id() { return this.user_id; }

    private void execute(String url, Callback successCallback, Callback errorCallback) {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("user_id", this.user_id));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();


    }
    public void save(String url, Callback successCallback) {
        this.execute(url, successCallback, (String error) -> {});
    }
    public void save(String url, Callback successCallback, Callback errorCallback) {
        this.execute(url, successCallback, errorCallback);
    }

    public static void getFromAPI(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public void addItem(Item item, String url, Callback successCallback, Callback errorCallback) {
         /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
         if (item.getProduct() == null) {
             data.add(new KeyValuePair("cart_id", this.id));
             data.add(new KeyValuePair("id", item.getSubcategory().getProductID()));
             data.add(new KeyValuePair("sub_category_id", item.getSubcategory().getID()));
             data.add(new KeyValuePair("quantity", String.valueOf(item.getQuantity())));
         } else {
             data.add(new KeyValuePair("cart_id", this.id));
             data.add(new KeyValuePair("id", item.getProduct().getId()));
             data.add(new KeyValuePair("sub_category_id", item.getProduct().getSubCategories().get(0).getID()));
             data.add(new KeyValuePair("quantity", String.valueOf(item.getQuantity())));
         }



        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public void pay(String status, String type, String phone_number, String address, String url, Callback successCallback, Callback errorCallback ) {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("cart_id", this.id));
        data.add(new KeyValuePair("status", status));
        data.add(new KeyValuePair("type", type));
        data.add(new KeyValuePair("phone_number", phone_number));
        data.add(new KeyValuePair("address", address));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public static void removeItem(String url, Callback successCallback, Callback errorCallback) {
        Cart.getFromAPI(url, successCallback, errorCallback);
    }
    public void updateAllItem(String url, Callback successCallback, Callback errorCallback) {
        /*
            Prepare data to send to API
         */

        for (Item item : this.getItems()) {
            ArrayList<KeyValuePair> data = new ArrayList<>();
            data.add(new KeyValuePair("item_id", item.getId()));
            data.add(new KeyValuePair("quantity", item.getQuantity()));
            data.add(new KeyValuePair("sub_category_id", item.getColor()));
            data.add(new KeyValuePair("size", item.getSize()));

            String requestType = "POST";

            AsyncAPI asyncAPI = new AsyncAPI()
                    .url(url)
                    .type(requestType)
                    .data(data)
                    .success((String response) -> successCallback.Invoke(response))
                    .error((String error) -> errorCallback.Invoke(error));

            asyncAPI.execute();
        }

    }
}
