package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;

public class Size {
    private String id;
    private String name;
    public Size() {}

    public String getID() { return this.id; }
    public String getName() { return this.name; }

    public static void getAllFromAPI(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
}
