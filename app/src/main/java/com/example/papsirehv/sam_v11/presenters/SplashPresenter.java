package com.example.papsirehv.sam_v11.presenters;

import android.util.Log;

import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class SplashPresenter {
    public interface View {
        void showLogin();
        void showHome();
    }

    private View view;
    public SplashPresenter(View view) {
        this.view = view;
    }
    public void checkUser(DatabaseHandler db) {
        User user = db.checkUser();
        if (user == null) {
            view.showLogin();
        } else {
            Global.User = user;
            checkCart();
            view.showHome();
        }
    }
    private void checkCart() {


        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        // create copy for presenter
                        //this.cart = cart;
                        Global.Cart = cart;


                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                        this.checkCart();
                    }
                },
                (String error) -> Log.d("s", "Error")
        );

    }

}
