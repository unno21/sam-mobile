package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.BaseActivity;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.cart_recyclerview.RecyclerViewType;
import com.example.papsirehv.sam_v11.cart_recyclerview.SectionModel;
import com.example.papsirehv.sam_v11.cart_recyclerview.SectionRecyclerViewAdapter;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.presenters.CartPresenter;
import com.example.papsirehv.sam_v11.utilities.Global;

import java.util.ArrayList;
import java.util.List;

public class CartActivity extends BaseActivity implements CartPresenter.View {

    public static final String RECYCLER_VIEW_TYPE = "recycler_view_type";
    private RecyclerViewType recyclerViewType;
    private RecyclerView recyclerView;
    private CartPresenter mCartPresenter;
    private TextView txtTotalItems, txtGrandTotal, txtVat;
    private Button btnCheckout;
    private Dialog cardPaymentDialog, paymentDialog, codPaymentDialog;

    @Override
    public int getContentViewId() {
        return R.layout.activity_cart;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_cart;
    }

    @Override
    public void childOnCreate() {


        //get enum type passed from MainActivity
        recyclerViewType = RecyclerViewType.LINEAR_VERTICAL;

//        setUpToolbarTitle();
        setUpRecyclerView();


        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Cart");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

//        mToolbar.setNavigationIcon(R.drawable.ic_back);
//        mToolbar.setNavigationOnClickListener(view -> finish());

        mCartPresenter = new CartPresenter(this);
        mCartPresenter.showCart();

        btnCheckout = findViewById(R.id.btnCheckout);
        txtTotalItems = findViewById(R.id.txtTotalItems);
        txtGrandTotal = findViewById(R.id.txtGrandTotal);
        txtVat = findViewById(R.id.txtVAT);

        paymentDialog = new Dialog(this);
        cardPaymentDialog = new Dialog(this);
        codPaymentDialog = new Dialog(this);

        if (Global.User.getCart() != null) {
            if (Global.User.getCart().getItems().size() == 0) {
                btnCheckout.setVisibility(View.GONE);
            } else {
                btnCheckout.setVisibility(View.VISIBLE);
            }
        } else {
            btnCheckout.setVisibility(View.VISIBLE);
        }

//        showColors();

//        btnCheckout.setOnClickListener(v -> {
//            this.showSuccessAlert("Checkout UI soon!");
//        });

    }

    //setup recycler view
    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void removeToCartClick(View v)
    {
        Dialog confirmDialog;

        confirmDialog = new Dialog(this);

        confirmDialog.setContentView(R.layout.custom_remove_from_cart);
        confirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmDialog.show();
    }


    @Override
    public void showCart(Cart cart, String image_path, int totalItems, double grandTotal) {
        ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
        sectionModelArrayList.add(new SectionModel(null, cart.getItems(), image_path));
        SectionRecyclerViewAdapter adapter = new SectionRecyclerViewAdapter(this, recyclerViewType, sectionModelArrayList);
        recyclerView.setAdapter(adapter);

        txtTotalItems.setText(String.valueOf(totalItems));
        txtVat.setText("₱ " + String.format("%.2f", grandTotal * 0.12));
        txtGrandTotal.setText("₱ " + grandTotal + "0");
    }

    @Override
    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    public void paymentClick(View view){
        mCartPresenter.updateItems();
        paymentDialog.setContentView(R.layout.custom_payment_dialog);
        paymentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        paymentDialog.show();

        Button btnCreditCard = paymentDialog.findViewById(R.id.btnCreditCard);
        Button btnCOD = paymentDialog.findViewById(R.id.btnCOD);

        btnCreditCard.setOnClickListener(v -> {
            creditCardPayment();
            paymentDialog.hide();
        });

        btnCOD.setOnClickListener(v -> {
            codPayment();
            paymentDialog.hide();
        });
    }

    public void creditCardPayment(){
        cardPaymentDialog.setContentView(R.layout.custom_credit_card_dialog);
        cardPaymentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cardPaymentDialog.show();

        EditText txtCardName = cardPaymentDialog.findViewById(R.id.txtCardName);
        EditText txtCardNumber = cardPaymentDialog.findViewById(R.id.txtCardNumber);
        EditText txtExpiration = cardPaymentDialog.findViewById(R.id.txtExpiration);
        EditText txtCvv = cardPaymentDialog.findViewById(R.id.txtCvv);
        Button btnPay = cardPaymentDialog.findViewById(R.id.btnPay);
        Button btnShop = cardPaymentDialog.findViewById(R.id.btnShop);
//
        btnPay.setOnClickListener(v -> {
            String cardName = txtCardName.getText().toString();
            String cardNumber = txtCardNumber.getText().toString();
            String expiration = txtExpiration.getText().toString();
            String cvv = txtCvv.getText().toString();
            mCartPresenter.checkOut("", "", cardName, cardNumber, expiration, cvv);
            cardPaymentDialog.hide();
        });
    }

    public void codPayment(){
        codPaymentDialog.setContentView(R.layout.custom_cod_dialog);
        codPaymentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        codPaymentDialog.show();

        EditText txtCODPhoneNumber = codPaymentDialog.findViewById(R.id.txtCODPhoneNumber);
        EditText txtCODAddress = codPaymentDialog.findViewById(R.id.txtCODAddress);
        Button btnPay = codPaymentDialog.findViewById(R.id.btnCODPay);

        btnPay.setOnClickListener(v -> {
            String phone_number = txtCODPhoneNumber.getText().toString();
            String address = txtCODAddress.getText().toString();

            if ((Global.User.getPhoneNumber() == null || Global.User.getPhoneNumber().equals("")) && phone_number.equals("")) {
                Toast.makeText(CartActivity.this, "Phone number is required.", Toast.LENGTH_SHORT).show();
            } else if ((Global.User.getAddress() == null || Global.User.getAddress().equals("")) && address.equals("")) {
                Toast.makeText(CartActivity.this, "Address is required.", Toast.LENGTH_SHORT).show();
            } else {
                mCartPresenter.checOutCOD(phone_number, address);
                codPaymentDialog.hide();
            }

        });
    }

//    public void showColors() {
//        Spinner sItems = (Spinner) findViewById(R.id.spnrColor);
//
//        List<String> spinnerColorArray =  new ArrayList<String>();
//        spinnerColorArray.add("White");
//        spinnerColorArray.add("Red");
//        spinnerColorArray.add("Green");
//        spinnerColorArray.add("Blue");
//        spinnerColorArray.add("Black");
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerColorArray);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        sItems.setAdapter(adapter);
//    }
}
