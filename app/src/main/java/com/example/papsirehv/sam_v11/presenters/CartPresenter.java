package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.APIResponse;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Item;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class CartPresenter {
    public interface View {
        void showCart(Cart cart, String image_path, int totalItems, double grandTotal);
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
    }
    private View view;
    private Cart cart;
    public CartPresenter(View view){ this.view = view; }

    public void showCart() {
        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        // create copy for presenter
                        this.cart = cart;
                        Global.Cart = cart;
                        String image_path = Global.STORAGE_PATH + "products/";

                        int totalItems = 0;
                        double grandTotal = 0;
                        for (Item item : cart.getItems()) {
                            totalItems += item.getQuantity();
                            grandTotal += item.getProduct().getPrice() * item.getQuantity();
                        }

                        view.showCart(this.cart, image_path, totalItems, grandTotal);
                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                        this.showCart();
                    }
                },
                (String error) -> view.showErrorAlert("")
        );
    }
    public void removeFromCart(Item item) {

        String url = Global.API_URL + "cart/remove/item/" + item.getId();
        Cart.removeItem(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        APIResponse response = gson.fromJson(result, APIResponse.class);

                        if (response.getResult().equals("success")) {
                            view.showSuccessAlert("Item has been removed from your cart.");
                            this.showCart();
                        } else {
                            view.showErrorAlert(response.getMessage());
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> {

                }
        );

    }

    public void checkOut(String phone_number, String address, String cardName, String cardNumber, String expiration, String cvv) {
        String validate = this.validateCheckOut(cardName, cardNumber, expiration, cvv);

        if (validate.equals("")) {
            Cart cart = Global.Cart;
            String status = "0";
            String type = "1";
            String url = Global.API_URL + "cart/pay";
            phone_number = phone_number.equals("") ? Global.User.getPhoneNumber() : phone_number;
            address = address.equals("") ? Global.User.getAddress() : address;

            cart.pay(
                    status,
                    type,
                    phone_number,
                    address,
                    url,
                    (String result) -> {
                        Gson gson = new Gson();
                        try {
                            APIResponse response = gson.fromJson(result, APIResponse.class);

                            if (response.getResult().equals("success")) {
                                view.showSuccessAlert(response.getMessage());
                                this.showCart();
                            } else {
                                view.showErrorAlert(response.getMessage());
                            }

                        } catch (Exception ex) {
                            //view.showErrorAlert(ex.toString());
                        }
                    },
                    (String error) -> view.showErrorAlert(error)
            );
        } else {
            view.showErrorAlert(validate);
        }
    }
    private String validateCheckOut(String cardName, String cardNumber, String expiration, String cvv) {
        if (cardName.equals("")) return "Card name field is required.";
        else if (cardNumber.equals("")) return "Card number field is required.";
        else if (expiration.equals("")) return "Expiration date field is required.";
        else if (cvv.equals("")) return "CVV field is required.";
        else return "";
    }
    public void checOutCOD(String phone_number, String address) {

        String status = "0";
        String type = "0";
        String url = Global.API_URL + "cart/pay";
        phone_number = phone_number.equals("") ? Global.User.getPhoneNumber() : phone_number;
        address = address.equals("") ? Global.User.getAddress() : address;

        cart.pay(
                status,
                type,
                phone_number,
                address,
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        APIResponse response = gson.fromJson(result, APIResponse.class);

                        if (response.getResult().equals("success")) {
                            view.showSuccessAlert(response.getMessage());
                            this.showCart();
                        } else {
                            view.showErrorAlert(response.getMessage());
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void updateItems(){
        String url = Global.API_URL + "cart/update/item";
        Cart cart = Global.Cart;

        cart.updateAllItem(
                url,
                (String result) -> {
                    //view.showSuccessAlert("Item has been updated.");
                },
                (String error) -> {

                }
        );

    }

    public void updateItem(Item item)
    {
        String url = Global.API_URL + "cart/update/item";
        item.save(
                url,
                (success) -> {

                },
                error -> {

                }
        );
    }


}
