package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class PreviousTransactionsPresenter {

    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showOrders(Order[] orders);
        void showOrderDetials();
    }

    private PreviousTransactionsPresenter.View view;
    private ArrayList<Order> orders;

    public PreviousTransactionsPresenter(PreviousTransactionsPresenter.View view){ this.view = view; }

    public void showOrders()
    {
        String url = Global.API_URL + "order/" + Global.User.getId();
        Order.getAll(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Order[] arrOrders = gson.fromJson(result, Order[].class);

                        // create copy for presenter
                        this.orders = new ArrayList<Order>(Arrays.asList(arrOrders));
                        view.showOrders(arrOrders);

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void showOrderDetails(int index)
    {
        Global.SelectedOrder = this.orders.get(index);
        this.view.showOrderDetials();
    }

}
