package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.daimajia.androidanimations.library.Techniques;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.presenters.SplashPresenter;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.viksaa.sssplash.lib.activity.AwesomeSplash;
import com.viksaa.sssplash.lib.cnst.Flags;
import com.viksaa.sssplash.lib.model.ConfigSplash;

public class SplashScreen extends AwesomeSplash implements SplashPresenter.View {

    private SplashPresenter mSplashPresenter;
    private DatabaseHandler db;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash_screen);
//
//        this.mSplashPresenter = new SplashPresenter(this);
//        db = new DatabaseHandler(this);
//
//
//        Thread timer = new Thread()
//        {
//            public void run()
//            {
//                try
//                {
//                    sleep(3000);
//                }
//                catch (InterruptedException e)
//                {
//                    e.printStackTrace();
//                }
//                finally
//                {
//                    mSplashPresenter.checkUser(db);
//                }
//            }
//        };
//        timer.start();
//    }

    @Override
    public void initSplash(ConfigSplash configSplash) {
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.hide();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Background Animation
        configSplash.setBackgroundColor(R.color.colorSplash);
        configSplash.setAnimCircularRevealDuration(2000);
        configSplash.setRevealFlagX(Flags.REVEAL_LEFT);
        configSplash.setRevealFlagX(Flags.REVEAL_BOTTOM);

        //Logo
        configSplash.setLogoSplash(R.drawable.img_splash_logo);
        configSplash.setAnimCircularRevealDuration(2000);
        configSplash.setAnimLogoSplashTechnique(Techniques.FadeIn);

        //Title
        configSplash.setTitleSplash("SHOPPING ASSISTANT MIRROR");
        configSplash.setTitleTextColor(R.color.colorSplashText);
        configSplash.setTitleTextSize(20f);
        configSplash.setAnimTitleDuration(2000);
        configSplash.setAnimTitleTechnique(Techniques.FadeIn);
    }


    @Override
    public void animationsFinished() {
        this.mSplashPresenter = new SplashPresenter(this);
        db = new DatabaseHandler(this);


        Thread timer = new Thread()
        {
            public void run()
            {
                try
                {
                    sleep(3000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    mSplashPresenter.checkUser(db);
                }
            }
        };
        timer.start();
    }

    @Override
    public void showLogin() {
        final   Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    @Override
    public void showHome() {
        final Intent HomeIntent = new Intent(this, HomeActivity.class);
        startActivity(HomeIntent);
        finish();
    }
}
