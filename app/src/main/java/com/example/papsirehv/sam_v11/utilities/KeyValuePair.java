package com.example.papsirehv.sam_v11.utilities;

public class KeyValuePair {
    private String key;
    private Object value;
    public KeyValuePair(String key, Object value) {
        this.key = key;
        this.value = value != null ? value : "";
    }
    public String getKey() { return this.key; }
    public Object getValue() { return this.value; }
}
