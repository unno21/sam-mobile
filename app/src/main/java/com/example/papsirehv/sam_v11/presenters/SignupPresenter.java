package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class SignupPresenter {
    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showVerification();
        void showHome();
        void enableSignUpButton();
        void disableSignUpButton();
    }

    private View view;
    public SignupPresenter(View view) { this.view = view; }

    public void register(String fullname, String username, String phonenumber, String email, String password, String confirmPassword){

         /*
            Create new user object
         */
        String[] name = (fullname).split(" ");
        User user;
        if (name.length > 2) {
            user = new User(name[0], name[1], name[2]);
        } else if (name.length > 1) {
            user = new User(name[0], " ", name[1]);
        }  else {
            user = new User(name[0], " ", " ");
        }
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhoneNumber(phonenumber);

        /*
            Validate input
         */
        String validationError = user.validate();

        if (!validationError.equals("")) {
            view.showErrorAlert(validationError);
        } else if (!password.equals(confirmPassword)) {
            validationError = "Password mismatch.";
            view.showErrorAlert(validationError);
        } else {

            view.disableSignUpButton();
            String url = Global.API_URL + "user/register";

            user.register(url,
                    (String result) -> {
                        view.enableSignUpButton();
                        Gson gson = new Gson();
                        try {
                            User loginUser = gson.fromJson(result, User.class);

                            if (loginUser.getId() != null) {
                                Global.User = loginUser;
                                view.showSuccessAlert("Registration Success");
                                view.showVerification();
                            } else {
                                view.showErrorAlert("Something went wrong");
                            }

                        } catch (Exception ex) {
                            view.showErrorAlert(ex.toString() + "\n");
                        }

                    },
                    (String error) -> view.showErrorAlert(error)
            );

        }

    }
    public void verify(String code)
    {
        User user = Global.User;
        user.setRandomCode(code);
        String url = Global.API_URL + "user/verify";

        user.verify(url,
                (String result) -> {

                    Gson gson = new Gson();
                    try {
                        User loginUser = gson.fromJson(result, User.class);

                        if (loginUser.getId() != null) {
                            Global.User = loginUser;

                            if (loginUser.getStatus() == 0) {
                                view.showErrorAlert("Invalid verification code.");
                            } else {
                                view.showSuccessAlert("Account has been verified.");
                                this.checkCart();
                                view.showHome();
                            }

                        } else {
                            view.showErrorAlert("Incorrect email/password");
                        }

                    } catch (Exception ex) {
                        view.showErrorAlert(ex.toString());
                    }

                },
                (String error) -> view.showErrorAlert(error)
        );

    }
    private void checkCart() {
        
        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        // create copy for presenter
                        //this.cart = cart;
                        Global.Cart = cart;


                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                        this.checkCart();
                    }
                },
                (String error) -> view.showErrorAlert("")
        );

    }
}
