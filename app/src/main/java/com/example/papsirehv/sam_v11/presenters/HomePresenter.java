package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Category;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Size;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class HomePresenter {
    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showProducts(Product[] products, String image_path);
        void showNewestProduct(Product product, String image_path);
        void showMostPopular(Product product, String image_path);
        void showProductDetails();
        void hideTopHomePage(String category);
        void showTopHomePage();
        void hideLoadMore();
        void showLoadMore();
    }

    private View view;
    private ArrayList<Product> products = new ArrayList<>();
    private ArrayList<Category> ProductCategories;
    private Product popular;
    private Product newest;

    private String productURL = Global.API_URL + "product/";

    public HomePresenter(View view){ this.view = view; }

    public void init()
    {

        loadMoreProducts();
        getSizes();
        getProductCategories();
        showNewestProduct();
        showPopularProduct();
        checkCart();

    }

    public void loadMoreProducts()
    {
        Product.getAllFromAPI(
                this.productURL  + this.products.size() + "/10",
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Product[] arrProducts = gson.fromJson(result, Product[].class);

                        // create copy for presenter
                        for(Product p : arrProducts) {
                            this.products.add(p);
                        }
                        //this.products = new ArrayList<Product>(Arrays.asList(arrProducts));
                        String image_path = Global.STORAGE_PATH + "products/";
                        view.showProducts(this.products.toArray(new Product[0]), image_path);

                        if (arrProducts.length < 10) {
                            view.hideLoadMore();
                        }


                    } catch (Exception ex) {
                        view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void showProductsByCategory(int index) {
        view.showLoadMore();
        this.products.clear();
        Category c = this.findCategory(index);
        if (c != null) {
            this.productURL = Global.API_URL + "product/filter/" + c.getId() + "/";
            loadMoreProducts();
            view.hideTopHomePage(c.getName().toUpperCase());
        }
    }

    public void showProductDetails(String productName) {
        int index = Integer.parseInt(productName.split(" ")[0]) - 1;
        Product selectedProduct = this.products.get(index);
        Global.SelectedProduct = selectedProduct;
        this.view.showProductDetails();
    }

    private void showNewestProduct() {
        String url = Global.API_URL + "product/newest";
        Product.getAllFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Product newestProduct = gson.fromJson(result, Product.class);
                        this.newest = newestProduct;

                        String image_path = Global.STORAGE_PATH + "products/";
                        view.showNewestProduct(newestProduct, image_path);
                    } catch (Exception ex) {
                       // view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    private void showPopularProduct() {
        String url = Global.API_URL + "product/popular";
        Product.getAllFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Product popularProduct = gson.fromJson(result, Product.class);
                        this.popular = popularProduct;

                        String image_path = Global.STORAGE_PATH + "products/";
                        view.showMostPopular(popularProduct, image_path);
                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void searchProduct(String keyword) {
        view.showLoadMore();
        this.products.clear();
        if (!keyword.equals("")) {
            this.productURL = Global.API_URL + "product/search/" + keyword+ "/";
            loadMoreProducts();
            view.hideTopHomePage("Search : " + keyword);
        }
    }

    public void showPopularDetails()
    {
        Global.SelectedProduct = popular;
        this.view.showProductDetails();
    }

    public void showNewestDetails()
    {
        Global.SelectedProduct = newest;
        this.view.showProductDetails();
    }

    public void resetURL()
    {
        if (!this.productURL.equals(Global.API_URL + "product/")) {
            this.products.clear();
            this.productURL = Global.API_URL + "product/";
            loadMoreProducts();
        }
    }

    private void getSizes()
    {
        String url = Global.API_URL + "product/size";
        Size.getAllFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Size[] arrSizes = gson.fromJson(result, Size[].class);

                        Global.Sizes = new ArrayList<Size>(Arrays.asList(arrSizes));

                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }
    private void getProductCategories()
    {
        String url = Global.API_URL + "category/product";
        Size.getAllFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Category[] arrCategories = gson.fromJson(result, Category[].class);

                        ProductCategories = new ArrayList<Category>(Arrays.asList(arrCategories));

                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }
    private Category findCategory(int index)
    {
        String[] categories = { "jacket", "polo shirt", "three fourth's", "t-shirt", "dress",
                "long sleeve", "skirt", "pants", "blouse", "short" };
        String toFind = categories[index];

        for (Category c : ProductCategories) {
            if (c.getName().equals(toFind)) return c;
        }

        return null;
    }

    private void checkCart() {
        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        Global.Cart = cart;

                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());

                    }
                },
                (String error) -> view.showErrorAlert("")
        );

    }

}
