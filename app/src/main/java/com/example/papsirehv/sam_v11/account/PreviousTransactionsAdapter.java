package com.example.papsirehv.sam_v11.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.views.PreviousTransactionsActivity;


public class PreviousTransactionsAdapter extends ArrayAdapter<Order> {

    public PreviousTransactionsAdapter(PreviousTransactionsActivity context, Order[] previousOrders) {
        super(context, R.layout.custom_transaction_row, previousOrders);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater previousTransactionsInflater = LayoutInflater.from(getContext());
        View customView = previousTransactionsInflater.inflate(R.layout.custom_transaction_row, parent, false);

        Order order = getItem(position);
        TextView txtOrderNumber = (TextView) customView.findViewById(R.id.txtOrderNumber);
        TextView txtTotal = customView.findViewById(R.id.txtOrderTotal);
        TextView txtDate = customView.findViewById(R.id.txtOrderDate);
        TextView txtOrderStatus = customView.findViewById(R.id.txtOrderStatus);

//        ImageView imgWishItemPic = (ImageView) customView.findViewById(R.id.imgWishItemPic);
//
        txtOrderNumber.setText(order.getOrder_number());
        txtTotal.setText("₱  " + String.valueOf(order.getTotal() + ".00"));
        txtDate.setText(order.getDate());
        txtOrderStatus.setText(order.getOrder_status());
//        imgWishItemPic.setImageResource(R.drawable.img_sample_garment);
        return customView;
    }
}
