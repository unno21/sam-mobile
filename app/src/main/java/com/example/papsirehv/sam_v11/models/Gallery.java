package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;

public class Gallery {

    private String id;

    private String user_id;

    private String image;

    public String getId() {
        return this.id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getImage() {
        return image;
    }

    public static void getAll(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public static void delete(String url, Callback successCallback, Callback errorCallback) {
        Gallery.getAll(url, successCallback, errorCallback);
    }

}
