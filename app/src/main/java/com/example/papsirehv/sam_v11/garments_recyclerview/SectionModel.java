package com.example.papsirehv.sam_v11.garments_recyclerview;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class SectionModel {
    private String sectionLabel;
    private ArrayList<String> names;
    private ArrayList<String> images;
    private ArrayList<String> categories;
    private ArrayList<String> prices;

    public SectionModel(String sectionLabel, ArrayList<String> names, ArrayList<String> categories,  ArrayList<String> prices, ArrayList<String> images) {
        this.sectionLabel = sectionLabel;
        this.names = names;
        this.images = images;
        this.categories = categories;
        this.prices = prices;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public ArrayList<String> getProductNames() {
        return this.names;
    }
    public ArrayList<String> getImages() {
        return this.images;
    }
    public ArrayList<String> getCategories() {
        return this.categories;
    }
    public ArrayList<String> getPrices() {
        return this.prices;
    }

}
