package com.example.papsirehv.sam_v11.garment_details_recyclerview;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemRecyclerViewAdapter.ItemViewHolder> {

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgProductDetails;

        public ItemViewHolder(View itemView) {
            super(itemView);
            imgProductDetails = itemView.findViewById(R.id.imgProductDetails);
            Global.GarmentDetailsImageView = imgProductDetails;
        }
    }

    private Context context;
    private ArrayList<String> arrayList;
    private Product product;

    public ItemRecyclerViewAdapter(Context context, ArrayList<String> arrayList, Product product) {
        this.context = context;
        this.arrayList = arrayList;
        this.product = product;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_details_custom_row_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Picasso.get().load(Global.STORAGE_PATH + "products/" + this.product.getImage()).into(holder.imgProductDetails);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


}
