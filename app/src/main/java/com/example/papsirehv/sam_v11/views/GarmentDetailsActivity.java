package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.garment_details.RecyclerViewType;
import com.example.papsirehv.sam_v11.garment_details.ReviewListAdapter;
import com.example.papsirehv.sam_v11.garment_details.SectionModel;
import com.example.papsirehv.sam_v11.garment_details.SectionRecyclerViewAdapter;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Review;
import com.example.papsirehv.sam_v11.models.SubCategory;
import com.example.papsirehv.sam_v11.presenters.GarmentDetailsPresenter;
import com.example.papsirehv.sam_v11.presenters.WishlistPresenter;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GarmentDetailsActivity extends AppCompatActivity implements GarmentDetailsPresenter.View {

    public static final String RECYCLER_VIEW_TYPE = "recycler_view_type";

    private RecyclerViewType recyclerViewType;
    private RecyclerView recyclerView;
    private GarmentDetailsPresenter mGarmentDetailsPresenter;

    private TextView lblProductName, lblPrice, lblCategory, lblRate, lblRateCount;
    private Button btnAddToCart;
    private ImageButton btnWishList, btnReviews;
    private Dialog reviewsDialog;
    private RatingBar userRatingBar;

    private Button btnYellow, btnBlue, btnPink, btnRed, btnOrange, btnPurple, btnBrown, btnGreen, btnOthers;
    ArrayList<Button> colorButtons = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_garment_details);

        //get enum type passed from MainActivity
        recyclerViewType = RecyclerViewType.LINEAR_HORIZONTAL;

//        setUpToolbarTitle();

        setUpRecyclerView();
//        showReviews();

        lblProductName = findViewById(R.id.lblProductName);
        lblPrice = findViewById(R.id.lblPrice);
        lblCategory = findViewById(R.id.lblCategory);
        lblRate = findViewById(R.id.lblRate);
        lblRateCount = findViewById(R.id.lblRateCount);
        btnAddToCart = findViewById(R.id.btnAddToCart);
        btnWishList = findViewById(R.id.btnWishList);
        btnReviews = findViewById(R.id.btnReviews);

        userRatingBar = findViewById(R.id.userRatingBar);

        btnYellow = findViewById(R.id.btnYellow);
        btnYellow.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("yellow"));
        colorButtons.add(btnYellow);
        btnBlue = findViewById(R.id.btnBlue);
        btnBlue.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("blue"));
        colorButtons.add(btnBlue);
        btnPink = findViewById(R.id.btnPink);
        btnPink.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("pink"));
        colorButtons.add(btnPink);
        btnRed = findViewById(R.id.btnRed);
        btnRed.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("red"));
        colorButtons.add(btnRed);
        btnOrange = findViewById(R.id.btnOrange);
        btnOrange.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("orange"));
        colorButtons.add(btnOrange);
        btnPurple = findViewById(R.id.btnPurple);
        btnPurple.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("purple"));
        colorButtons.add(btnPurple);
        btnBrown = findViewById(R.id.btnBrown);
        btnBrown.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("brown"));
        colorButtons.add(btnBrown);
        btnGreen = findViewById(R.id.btnGreen);
        btnGreen.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("green"));
        colorButtons.add(btnGreen);
        btnOthers = findViewById(R.id.btnOthers);
        btnOthers.setOnClickListener(v -> mGarmentDetailsPresenter.changeColor("others"));
        colorButtons.add(btnOthers);

        mGarmentDetailsPresenter = new GarmentDetailsPresenter(this);
        mGarmentDetailsPresenter.showProductDetails();

        reviewsDialog = new Dialog(this);

    }

    //setup recycler view
    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.garmentdetails_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProductDetails(Product product) {


        ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();

        ArrayList<String> itemArrayList = new ArrayList<>();

//        if (product.getSubCategories().size() > 0) {
//            for (int j = 1; j <= product.getSubCategories().size(); j++) {
//                itemArrayList.add("Item " + j);
//            }
//        } else {
//            itemArrayList.add("Item " + 1);
//        }

        itemArrayList.add("Item " + 0);


        sectionModelArrayList.add(new SectionModel(null, itemArrayList, product));

        SectionRecyclerViewAdapter adapter = new SectionRecyclerViewAdapter(this, recyclerViewType, sectionModelArrayList);
        recyclerView.setAdapter(adapter);

        lblProductName.setText(product.getName());
        lblPrice.setText("₱ " + String.valueOf(product.getPrice()));
        lblCategory.setText(product.getCategory().getName());
        userRatingBar.setRating(product.getStarRate());
        lblRate.setText(String.valueOf(product.getStarRate()) + "/5");
        lblRateCount.setText(String.valueOf(product.getReviews().size()) + " customer rating(s)");

        btnAddToCart.setOnClickListener(v -> {
            mGarmentDetailsPresenter.addToCart();
        });

        btnWishList.setOnClickListener(v -> {
            if (btnWishList.getBackground().getConstantState() == getResources().getDrawable(R.drawable.img_heart1).getConstantState()){
                mGarmentDetailsPresenter.addToWish(product);
                changeHeartStatus(true);
            }
            else if (btnWishList.getBackground().getConstantState() == getResources().getDrawable(R.drawable.img_heart2).getConstantState()){
                mGarmentDetailsPresenter.removeFromWish(product);
                changeHeartStatus(false);
            }
        });

        mGarmentDetailsPresenter.checkWishList();

    }

    @Override
    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void changeHeartStatus(Boolean inWishList) {
        if (inWishList) {
            btnWishList.setBackgroundResource(R.drawable.img_heart2);
        } else {
            btnWishList.setBackgroundResource(R.drawable.img_heart1);
        }
    }

    @Override
    public void changeImage(SubCategory subCategory) {
        Picasso.get().load(Global.STORAGE_PATH + "products/" + subCategory.getImage()).into(Global.GarmentDetailsImageView);
    }

    @Override
    public void hideColorButtons(int[] index) {
        for(int i : index) {
            colorButtons.get(i).setVisibility(View.GONE);
        }
    }

    public void showReviews(View view){

        reviewsDialog.setContentView(R.layout.custom_reviews_dialog);
        reviewsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        reviewsDialog.show();

        Review[] reviews = mGarmentDetailsPresenter.getProductReviews().toArray(new Review[0]);
        ListAdapter reviewListAdapter = new ReviewListAdapter(this, reviews);
        ListView reviewListView = reviewsDialog.findViewById(R.id.lstReviews);
        reviewListView.setAdapter(reviewListAdapter);
    }
}
