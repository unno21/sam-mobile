package com.example.papsirehv.sam_v11.views;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.BaseActivity;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.cart.CartAdapter;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Wishlist;
import com.example.papsirehv.sam_v11.presenters.WishlistPresenter;
import com.example.papsirehv.sam_v11.wishlist.WishListAdapter;

public class WishListActivity extends BaseActivity implements WishlistPresenter.View {

    private WishlistPresenter mWishlistPresenter;
    @Override
    public int getContentViewId() {
        return R.layout.activity_wish_list;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_wishlist;
    }

    @Override
    public void childOnCreate() {



        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Wishlist");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

//        mToolbar.setNavigationIcon(R.drawable.ic_back);

//        mToolbar.setNavigationOnClickListener(view -> finish());

        mWishlistPresenter = new WishlistPresenter(this);
        mWishlistPresenter.showWishlist();



//        wishListView.setOnItemClickListener(
//                (parent, view, position, id) -> {
//                    String cart1 = String.valueOf(parent.getItemAtPosition(position));
////                        Toasty.normal(CartActivity.this, wish, Toast.LENGTH_SHORT).show();
//                }
//        );

    }

    @Override
    public void showWishlist(Wishlist[] wishlist, String image_path) {
        int size = wishlist.length;
//        Product[] products = new Product[size];
//
//        for (int i = 0; i < size; i++) {
//            products[i] = wishlist[i].getProduct();
//        }

        ListAdapter wishListAdapter = new WishListAdapter(this, wishlist);
        ListView wishListView = findViewById(R.id.lstWishList);
        wishListView.setAdapter(wishListAdapter);
    }

    @Override
    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }
}
