package com.example.papsirehv.sam_v11.models;

import android.telecom.Call;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class User {
    private String id;
    private String first_name, middle_name, last_name, extension_name;
    private String address;
    private String image;
    private String email, username, password, phone_number;
    private String type;
    private String random_code;
    private int status;
    private String created_at, updated_at;
    private Cart cart;

    public User(String first_name, String middle_name, String last_name) {
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
    }
    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
    public User() {}

    public void setId(String id) { this.id = id; }
    public String getId() { return this.id; }
    public void setFirstName(String first_name) { this.first_name = first_name; }
    public String getFirstName() { return this.first_name; }
    public void setMiddleName(String middle_name) { this.middle_name = middle_name; }
    public String getMiddleName() { return this.middle_name; }
    public void setLastName(String lastName) { this.last_name = lastName; }
    public String getLastName() { return this.last_name; }
    public void setExtensionName(String extension_name) { this.extension_name = extension_name; }
    public String getExtensionName() { return this.extension_name; }
    public void setImage(String image) { this.image = image; }
    public String getImage() { return this.image; }
    public void setEmail(String email) { this.email = email; }
    public String getEmail() { return this.email; }
    public void setUsername(String username) { this.username = username; }
    public String getUsername() { return this.username; }
    public void setPassword(String password) { this.password = password; }
    public String getPassword() { return this.password; }
    public void setPhoneNumber(String phone_number) { this.phone_number = phone_number; }
    public String getPhoneNumber() { return this.phone_number; }
    public void setType(String type) { this.type = type; }
    public String getType() { return this.type; }
    public void setRandomCode(String code) { this.random_code = code; }
    public String getRandomCode() { return this.random_code; }
    public void setStatus(int status) { this.status = status; }
    public int getStatus() { return this.status; }
    public String getCreated_at() { return this.created_at; }
    public String getUpdated_at() { return this.updated_at; }
    public void setCart(Cart cart) { this.cart = cart; }
    public Cart getCart() { return this.cart; }
    public void setAddress(String address) { this.address = address; }
    public String getAddress() { return this.address; }
    public String getFullName() {
        String fullname = "";
        fullname += this.first_name == null ? "" : this.first_name + " ";
        fullname += this.middle_name == null ? "" : this.middle_name + " ";
        fullname += this.last_name == null ? "" : this.last_name + " ";
        fullname += this.extension_name == null ? "" : this.extension_name + " ";
        return fullname;
    }

    private void execute(String url, Callback successCallback, Callback errorCallback) {

        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("first_name", this.first_name));
        data.add(new KeyValuePair("middle_name", this.middle_name));
        data.add(new KeyValuePair("last_name", this.last_name));
        data.add(new KeyValuePair("username", this.username));
        data.add(new KeyValuePair("email", this.email));
        data.add(new KeyValuePair("password", this.password));
        data.add(new KeyValuePair("address", this.address));
        data.add(new KeyValuePair("phone_number", this.phone_number));
        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public void save(String url, Callback successCallback) {
        this.execute(url, successCallback, (String error) -> {});
    }
    public void save(String url, Callback successCallback, Callback errorCallback) {
        this.execute(url, successCallback, errorCallback);
    }

    public void getRecordsFromAPI(String url, Callback successCallback, Callback errorCallback) {

        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();

    }
    public void getRecordsFromAPI(String url, Callback successCallback) {
        this.getRecordsFromAPI(url, successCallback, (String error) -> {});
    }

    public void login(String url, Callback successCallback, Callback errorCallback){
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("email", this.email));
        data.add(new KeyValuePair("password", this.password));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();

    }
    public void login(String url, Callback successCallback){
        this.login(url, successCallback, (String error) -> {});
    }

    public void loginToMirror(String url, Callback successCallback, Callback errorCallback) {
        url += "/" + this.id;
        String requestType = "GET";
        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public void loginByCode(String url, Callback successCallback)
    {
        String requestType = "GET";
        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response));

        asyncAPI.execute();
    }

    public void register(String url, Callback successCallback, Callback errorCallback) {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("first_name", this.first_name));
        data.add(new KeyValuePair("middle_name", this.middle_name));
        data.add(new KeyValuePair("last_name", this.last_name));
        data.add(new KeyValuePair("username", this.username));
        data.add(new KeyValuePair("phone_number", this.phone_number));
        data.add(new KeyValuePair("email", this.email));
        data.add(new KeyValuePair("password", this.password));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public void register(String url, Callback successCallback){
        this.register(url, successCallback, (String error) -> {});
    }
    public void verify(String url, Callback successCallback, Callback errorCallback)
    {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("code", this.random_code));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public static void resetPassword(String url, Callback successCallback, Callback errorCallback) {

        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public void changePassword(String currentPassword, String newPassword, String url, Callback successCallback, Callback errorCallback)
    {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("email", this.email));
        data.add(new KeyValuePair("password", currentPassword));
        data.add(new KeyValuePair("new_password", newPassword));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }


    public String validate()
    {
        if (this.first_name.length() < 2) { return "Name field is required!"; }
        else if (this.username == null || this.username.equals("")) { return "Username field is required!"; }
        else if (this.email == null || this.email.equals("")) { return "Email field is required!"; }
        else if (this.password == null || this.password.equals("")) { return "Password field is required!"; }
        else return "";
    }

    public void resendCode(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";
        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

}
