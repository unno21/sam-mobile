package com.example.papsirehv.sam_v11.utilities;

import android.os.AsyncTask;

import com.example.papsirehv.sam_v11.interfaces.Callback;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class AsyncAPI extends AsyncTask<String, Void, String> {

    private String urlStr;
    private String type;
    private ArrayList<KeyValuePair> data;
    private Callback successCallback;
    private Callback errorCallback;

    private String error;

    public AsyncAPI url(String url) { this.urlStr = url; return this; }
    public AsyncAPI type(String type) { this.type = type; return this; }
    public AsyncAPI data(ArrayList<KeyValuePair> data) {  this.data = data; return this; }
    public AsyncAPI success(Callback successCallback) { this.successCallback = successCallback; return this; }
    public AsyncAPI error(Callback errorCallback) { this.errorCallback = errorCallback; return this; }

    @Override
    protected String doInBackground(String... strings) {

        try {

            URL url = new URL(this.urlStr);
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
            httpURLConnection.setRequestMethod(this.type);

            if (this.type.toUpperCase().equals("POST")) {
                httpURLConnection.setDoOutput(true);
            }

            /*
                check if there is data to pass to API
             */
            if (this.data != null) {
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                String dataToSend = "";
                for (KeyValuePair k : data) {
                    dataToSend += URLEncoder.encode(k.getKey(), "UTF-8") + "=" + URLEncoder.encode(k.getValue().toString()) + "&";
                }
                // remove the last '&'
                dataToSend = dataToSend.substring(0, dataToSend.length() - 1);

                bufferedWriter.write(dataToSend);
                bufferedWriter.flush();
                bufferedWriter.close();
                os.close();
            }


            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
            String response = "";
            String line = "";

            while ((line = bufferedReader.readLine()) != null){
                response += line;
            }

            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();

            return response;
        } catch (MalformedURLException ex){
            error = ex.toString();
        } catch (IOException ex) {
            error = ex.toString();
        } catch (Exception ex) {
            error = ex.toString();
        }
        return null;
    }
    @Override
    protected void onPostExecute(String result) {

        if (error == null) {
            successCallback.Invoke(result);
        } else if ((errorCallback != null) && (error != null)){
            errorCallback.Invoke(error);
        }

    }
}
