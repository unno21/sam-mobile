package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.media.Image;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.BaseActivity;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.garments_recyclerview.RecyclerViewType;
import com.example.papsirehv.sam_v11.garments_recyclerview.SectionModel;
import com.example.papsirehv.sam_v11.garments_recyclerview.SectionRecyclerViewAdapter;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.presenters.HomePresenter;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import pl.droidsonroids.gif.GifImageView;


public class HomeActivity extends BaseActivity implements HomePresenter.View {

    public static final String RECYCLER_VIEW_TYPE = "recycler_view_type";

    private RecyclerViewType recyclerViewType;
    private RecyclerView recyclerView;
    private HomePresenter mHomePresenter;
    private EditText txtSearchProduct;
    private Button btnSearchProduct;
    private TextView btnLoadMore;

    @Override
    public int getContentViewId() {
        return R.layout.activity_home;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_home;
    }

    @Override
    public void childOnCreate() {

        //get enum type passed from MainActivity
        recyclerViewType = RecyclerViewType.GRID;

//        setUpToolbarTitle();
        setUpRecyclerView();
        mHomePresenter = new HomePresenter(this);
        mHomePresenter.init();

        txtSearchProduct = findViewById(R.id.txtSearchProduct);
        btnSearchProduct = findViewById(R.id.btnSearchProduct);
        btnLoadMore = findViewById(R.id.btnShowMore);

        btnSearchProduct.setOnClickListener(v -> {
            String keyword = txtSearchProduct.getText().toString();
            mHomePresenter.searchProduct(keyword);
        });

        btnLoadMore.setOnClickListener(v -> {
            mHomePresenter.loadMoreProducts();
        });

        ArrayList<ImageView> imgCategories = new ArrayList<>();
        imgCategories.add(findViewById(R.id.imgJacket));
        imgCategories.add(findViewById(R.id.imgPoloShirt));
        imgCategories.add(findViewById(R.id.imgThreeFourth));
        imgCategories.add(findViewById(R.id.imgTShirt));
        imgCategories.add(findViewById(R.id.imgDress));
        imgCategories.add(findViewById(R.id.imgLongSleeve));
        imgCategories.add(findViewById(R.id.imgSkirt));
        imgCategories.add(findViewById(R.id.imgPants));
        imgCategories.add(findViewById(R.id.imgBlouse));
        imgCategories.add(findViewById(R.id.imgShort));

        for (ImageView img : imgCategories) {
            img.setOnClickListener( v -> {
                ImageView imgCategory = (ImageView)v;
                int index = imgCategories.indexOf(imgCategory);
                mHomePresenter.showProductsByCategory(index);
            });
        }

    }

    //setup recycler view
    private void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.sectioned_recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void itemClick(View view) {
        TextView lblProductName = view.findViewById(R.id.lblProductName);
        String productName = lblProductName.getText().toString();
        mHomePresenter.showProductDetails(productName);
    }

    @Override
    public void showSuccessAlert(String message) {
//        Toasty.success(this.getApplicationContext(),message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProducts(Product[] products, String image_path) {
        ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();

        ArrayList<String> names = new ArrayList<>();
        ArrayList<String> categories = new ArrayList<>();
        ArrayList<String> prices = new ArrayList<>();
        ArrayList<String> images = new ArrayList<>();

        for(Product product: products) {
            names.add(product.getName());
            categories.add(product.getCategory().getName());
            prices.add(String.valueOf(product.getPrice()));
            images.add(image_path + product.getImage());
        }
        sectionModelArrayList.add(new SectionModel(null, names, categories, prices, images));

        SectionRecyclerViewAdapter adapter = new SectionRecyclerViewAdapter(this, recyclerViewType, sectionModelArrayList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showNewestProduct(Product product, String image_path) {
        GifImageView imgNewest = findViewById(R.id.imgNewest);
        TextView lblNewest = findViewById(R.id.lblNewest);

        Picasso.get().load(image_path +  product.getImage()).into(imgNewest);
        lblNewest.setText(product.getName());

        imgNewest.setOnClickListener(v -> {
            mHomePresenter.showNewestDetails();
        });

    }

    @Override
    public void showMostPopular(Product product, String image_path) {
        GifImageView imgPopular = findViewById(R.id.imgPopular);
        TextView lblPopular = findViewById(R.id.lblPopular);

        Picasso.get().load(image_path +  product.getImage()).into(imgPopular);
        //Picasso.get().load("http://192.168.0.108/survey/public/respondent/qr/9").into(imgPopular);
        lblPopular.setText(product.getName());

        imgPopular.setOnClickListener(v -> {
            mHomePresenter.showPopularDetails();
        });
    }

    @Override
    public void showProductDetails() {
        startActivity(new Intent(this, GarmentDetailsActivity.class));
    }

    @Override
    public void hideTopHomePage(String category) {
        LinearLayout topHomePage = findViewById(R.id.topHomePage);
        topHomePage.setVisibility(View.GONE);
        TextView lblCategory = findViewById(R.id.lblCategory);
        lblCategory.setText(category);
    }

    @Override
    public void showTopHomePage() {
        LinearLayout topHomePage = findViewById(R.id.topHomePage);
        topHomePage.setVisibility(View.VISIBLE);
        TextView lblCategory = findViewById(R.id.lblCategory);
        lblCategory.setText("DAILY DISCOVER");
    }

    @Override
    public void hideLoadMore() {
        btnLoadMore.setVisibility(View.GONE);
    }

    @Override
    public void showLoadMore() {
        btnLoadMore.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        this.showTopHomePage();
        this.showLoadMore();
        mHomePresenter.resetURL();
    }
}
