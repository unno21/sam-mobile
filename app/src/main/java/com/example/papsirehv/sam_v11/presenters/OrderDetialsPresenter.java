package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.APIResponse;
import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class OrderDetialsPresenter {

    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showOrderDetails(Order order, String imagePath);
        void showOrders();
        void hideCancelButton();
        void setBubbleThumbSeekbarValue(int value);
        void setBubbleThumbSeekbarColor(String color);
        void hideCancelLabel();
        void showCancelLabel();
        void hideBubbleThumbSeekbar();
        void showBubbleThumbSeekbar();
    }

    private OrderDetialsPresenter.View view;

    public OrderDetialsPresenter(OrderDetialsPresenter.View view){ this.view = view; }

    public void showOrderDetails()
    {
        String imagePath = Global.STORAGE_PATH + "products/";
        Order order = Global.SelectedOrder;
        this.view.showOrderDetails(order, imagePath);

        if(!order.getOrder_status().equals("Process")) {
            this.view.hideCancelButton();
        }

        if(order.getOrder_status().equals("Cancelled")) {
//            this.view.setBubbleThumbSeekbarColor("#d83737");
            this.view.showCancelLabel();
//            this.view.setBubbleThumbSeekbarValue(33);
        } else {
//            this.view.setBubbleThumbSeekbarColor("#3853D8");
            this.view.hideCancelLabel();

            if (order.getOrder_status().equals("Process")) {
                this.view.setBubbleThumbSeekbarValue(0);
            } else if (order.getOrder_status().equals("Shipped")) {
                this.view.setBubbleThumbSeekbarValue(50);
            } else if (order.getOrder_status().equals("Delivered")) {
                this.view.setBubbleThumbSeekbarValue(100);
            }
        }
    }

    public void cancelOrder()
    {
        Order order = Global.SelectedOrder;
        String url = Global.API_URL + "order/cancel/" + order.getId();
        order.cancel(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        APIResponse response = gson.fromJson(result, APIResponse.class);

                        if (response.getResult().equals("success")) {
                            view.showSuccessAlert(response.getMessage());
                            view.showOrders();
                        } else {
                            view.showErrorAlert(response.getMessage());
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> view.showErrorAlert(error)
        );
    }

}
