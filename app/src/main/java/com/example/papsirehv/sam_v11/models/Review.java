package com.example.papsirehv.sam_v11.models;

public class Review {
    private String user_id, product_id;
    private String comment;
    private int rate;
    private User user;

    public Review() {}

    public void setUser(User user) { this.user = user; }
    public User getUser() { return this.user; }
    public void setComment(String comment) { this.comment = comment; }
    public String getComment() { return this.comment; }
    public void setRate(int rate) { this.rate = rate; }
    public int getRate() { return this.rate; }
}
