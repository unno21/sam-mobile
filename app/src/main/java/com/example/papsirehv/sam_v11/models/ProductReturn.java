package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class ProductReturn {

    private String id;

    private String product_id;

    private String order_detail_id;

    private String buyer_note;

    private int quantity;

    public String getId() {
        return id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getOrder_detail_id() {
        return order_detail_id;
    }

    public String getBuyer_note() {
        return buyer_note;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setOrder_detail_id(String order_detail_id) {
        this.order_detail_id = order_detail_id;
    }

    public void setBuyer_note(String buyer_note) {
        this.buyer_note = buyer_note;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void save(String url, Callback successCallback, Callback errorCallback)
    {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("product_id", this.product_id));
        data.add(new KeyValuePair("order_id", this.order_detail_id));
        data.add(new KeyValuePair("buyer_note", this.buyer_note));
        data.add(new KeyValuePair("quantity", this.quantity));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
}
