package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.example.papsirehv.sam_v11.views.AddressListFragment;
import com.example.papsirehv.sam_v11.BaseActivity;
//import com.example.papsirehv.sam_v11.PersonalInfoFragment;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.account.AccountSettingsAdapter;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.lang.reflect.Array;

public class AccountActivity extends BaseActivity {

    @Override
    public int getContentViewId() {
        return R.layout.activity_account;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_account;
    }

    @Override
    public void childOnCreate() {

        String[] accountSettings = {"Personal Information","Recent Orders", "Login To SAM", "Logout"};
        ListAdapter accountSettingsAdapter = new AccountSettingsAdapter(this, accountSettings);
        ListView accountSettingsListView = findViewById(R.id.lstAccountSettings);
        accountSettingsListView.setAdapter(accountSettingsAdapter);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Account Settings");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

//        mToolbar.setNavigationIcon(R.drawable.ic_back);
//        mToolbar.setNavigationOnClickListener(view -> finish());




        accountSettingsListView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {

//            Toast.makeText(getApplicationContext(), (String) accountSettingsListView.getItemAtPosition(position), Toast.LENGTH_SHORT).show();

            String selectedItem = (String) accountSettingsListView.getItemAtPosition(position);
            Fragment selectedFragment = null;

            switch (selectedItem.toString()) {
                case "Personal Information":
                    selectedFragment = new PersonalInfoFragment();
                    break;

                case "Recent Orders":
                    startActivity(new Intent(this, PreviousTransactionsActivity.class));
                    break;

                case "Login To SAM":
                    IntentIntegrator integrator = new IntentIntegrator(this);
                    integrator.setOrientationLocked(false);
                    integrator.initiateScan();
                    break;

                case "Logout":
                    (new DatabaseHandler(this)).deleteAll();
                    Global.User = null;
                    startActivity(new Intent(AccountActivity.this, LoginActivity.class));
                    finish();
                    break;
            }

            if (!selectedItem.toString().equals("Recent Orders") && !selectedItem.toString().equals("Login To SAM")){
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();
            }



        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();

            } else {
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                String qr_code =  result.getContents();
                User user = Global.User;
                user.loginToMirror(qr_code, res -> {
                    //
                            Gson gson = new Gson();
                            try {
                                User loginUser = gson.fromJson(res, User.class);
                                Toast.makeText(this,"Login to mirror success.", Toast.LENGTH_LONG).show();

                            } catch (Exception ex) {
                                Toast.makeText(this,"Invalid QR code.", Toast.LENGTH_LONG).show();
                            }
                },
                        error -> {
                        Toast.makeText(this, "An error occured.", Toast.LENGTH_LONG).show();
                    }
                );
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
