package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class ProductReview {

    private String id = "0";

    private String user_id;

    private String product_id;

    private String comment;

    private float rate;

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getComment() {
        return comment;
    }

    public float getRate() {
        return rate;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public static void get(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public void save(String url, Callback successCallback, Callback errorCallback)
    {
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("user_id", this.user_id));
        data.add(new KeyValuePair("product_id", this.product_id));
        data.add(new KeyValuePair("comment", this.comment));
        data.add(new KeyValuePair("rate", this.rate));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
}
