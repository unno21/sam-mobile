package com.example.papsirehv.sam_v11.utilities;

import android.widget.ImageView;

import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.models.OrderDetail;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Size;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Wishlist;

import java.util.ArrayList;

public abstract class
Global {
    public static User User;
//    public static String API_URL = "http://169.254.185.49/sam-web/public/api/";
//    public static String STORAGE_PATH = "http://169.254.185.49/sam-web/public/storage/";
    public static String API_URL = "http://www.sam-department.com/samapparel/public/api/";
    public static String STORAGE_PATH = "http://www.sam-department.com/samapparel/public/storage/";

//    public static String API_URL = "http://192.168.0.108/sam/public/api/";
//    public static String STORAGE_PATH = "http://192.168.0.108/sam/public/storage/";

    public static Product SelectedProduct;
    public static Cart Cart;
    public static ArrayList<Wishlist> Wishlists;
    public static ArrayList<Size> Sizes;
    public static Order SelectedOrder;
    public static OrderDetail SelectedOrderDetail;

    //daya
    public static ImageView GarmentDetailsImageView;
}
