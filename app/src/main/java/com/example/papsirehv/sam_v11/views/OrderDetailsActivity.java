package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.widgets.BubbleThumbSeekbar;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.account.OrderListAdapter;
import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.models.OrderDetail;
import com.example.papsirehv.sam_v11.presenters.OrderDetialsPresenter;

public class OrderDetailsActivity extends AppCompatActivity implements OrderDetialsPresenter.View {

    private OrderDetialsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Order Details");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setNavigationOnClickListener(view -> finish());


        mPresenter = new OrderDetialsPresenter(this);
        mPresenter.showOrderDetails();

    }

    @Override
    public void showSuccessAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOrderDetails(Order order, String imagePath) {

        TextView lblOrderNumber = findViewById(R.id.lblOrderNumber);
        TextView lblOrderTotal = findViewById(R.id.lblOrderTotal);
        TextView lblOrderDate = findViewById(R.id.lblOrderDate);
        Button btnOderDetailsShopAgain = findViewById(R.id.btnOderDetailsShopAgain);
        Button btnCancelOrder = findViewById(R.id.btnCancelOrder);

        lblOrderNumber.setText(order.getOrder_number());
        lblOrderTotal.setText("₱  " + String.valueOf(order.getTotal() + ".00"));
        lblOrderDate.setText(order.getDate());

        btnOderDetailsShopAgain.setOnClickListener(v -> {
            startActivity(new Intent(OrderDetailsActivity.this, HomeActivity.class));
            finish();
        });

        btnCancelOrder.setOnClickListener(v -> {
            mPresenter.cancelOrder();
        });

        ListAdapter orderListAdapter = new OrderListAdapter(this, order.getOrder_details(),imagePath);
        ListView orderListView = findViewById(R.id.lstOrders);
        orderListView.setAdapter(orderListAdapter);
    }

    @Override
    public void showOrders() {
        startActivity(new Intent(OrderDetailsActivity.this, PreviousTransactionsActivity.class));
        finish();
    }

    @Override
    public void hideCancelButton() {
        Button btnCancelOrder = findViewById(R.id.btnCancelOrder);
        btnCancelOrder.setVisibility(View.GONE);
    }

    @Override
    public void setBubbleThumbSeekbarValue(int value) {
        BubbleThumbSeekbar rangeSeekbar = findViewById(R.id.rangeSeekbar3);
        rangeSeekbar.setMinStartValue(value);
        rangeSeekbar.apply();
    }

    @Override
    public void setBubbleThumbSeekbarColor(String color) {
        BubbleThumbSeekbar rangeSeekbar = findViewById(R.id.rangeSeekbar3);
        rangeSeekbar.setBarHighlightColor(Color.parseColor(color));
        rangeSeekbar.apply();
    }

    @Override
    public void hideCancelLabel() {
        findViewById(R.id.lblOrderDetailCanceled).setVisibility(View.GONE);
        findViewById(R.id.lblProcessing).setVisibility(View.VISIBLE);
        findViewById(R.id.lblShipped).setVisibility(View.VISIBLE);
        findViewById(R.id.lblDelivered).setVisibility(View.VISIBLE);
    }

    @Override
    public void showCancelLabel() {
        findViewById(R.id.lblOrderDetailCanceled).setVisibility(View.VISIBLE);
        findViewById(R.id.lblProcessing).setVisibility(View.GONE);
        findViewById(R.id.lblShipped).setVisibility(View.GONE);
        findViewById(R.id.lblDelivered).setVisibility(View.GONE);
    }

    @Override
    public void hideBubbleThumbSeekbar() {
        findViewById(R.id.rangeSeekbar3).setVisibility(View.GONE);
    }

    @Override
    public void showBubbleThumbSeekbar() {
        findViewById(R.id.rangeSeekbar3).setVisibility(View.VISIBLE);
    }

}
