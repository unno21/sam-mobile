package com.example.papsirehv.sam_v11.garment_details;

import com.example.papsirehv.sam_v11.models.Product;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class SectionModel {
    private String sectionLabel;
    private ArrayList<String> itemArrayList;
    private Product product;
    public SectionModel(String sectionLabel, ArrayList<String> itemArrayList, Product product) {
        this.sectionLabel = sectionLabel;
        this.itemArrayList = itemArrayList;
        this.product = product;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public ArrayList<String> getItemArrayList() {
        return itemArrayList;
    }
    public Product getProduct() { return this.product; }
}
