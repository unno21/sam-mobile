package com.example.papsirehv.sam_v11.cart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;

public class CartAdapter extends ArrayAdapter<String> {

    public CartAdapter(Context context, String[] garments) {
        super(context, R.layout.custom_cart_row, garments);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater cartInflater = LayoutInflater.from(getContext());
        View customView = cartInflater.inflate(R.layout.custom_cart_row, parent, false);

        String singleCartItem = getItem(position);
        TextView txtCartProductName = (TextView) customView.findViewById(R.id.txtCartProductName);
        ImageView imgCartProductImage = (ImageView) customView.findViewById(R.id.imgCartProductImage);
        //Button btnRemove = (Button) customView.findViewById(R.id.btnRemove);

        txtCartProductName.setText(singleCartItem);
        imgCartProductImage.setImageResource(R.drawable.img_sample_garment);

        return customView;
    }
}
