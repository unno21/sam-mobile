package com.example.papsirehv.sam_v11.wishlist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.SubCategory;
import com.example.papsirehv.sam_v11.models.Wishlist;
import com.example.papsirehv.sam_v11.presenters.WishlistPresenter;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class WishListAdapter extends ArrayAdapter<Wishlist> {

    public WishListAdapter(Context context, Wishlist[] wishlists) {
        super(context, R.layout.custom_wishlist_row, wishlists);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater cartInflater = LayoutInflater.from(getContext());
        View customView = cartInflater.inflate(R.layout.custom_wishlist_row, parent, false);

        Wishlist singleWishItem = getItem(position);
        TextView txtWishProductName = customView.findViewById(R.id.txtWishProductName);
        TextView txtWishCategory = customView.findViewById(R.id.txtWishCategory);
        TextView txtWishProductPrice = customView.findViewById(R.id.txtWishProductPrice);
        ImageView imgWishItemPic =  customView.findViewById(R.id.imgWishItemPic);
        Button btnRemoveFromWish = customView.findViewById(R.id.btnRemoveFromWish);
        Button btnWishAddToCart = customView.findViewById(R.id.btnWishAddToCart);
        Spinner sprSize = customView.findViewById(R.id.spnrSize);
        Spinner sprColor = customView.findViewById(R.id.spnrColor);

        txtWishProductName.setText(singleWishItem.getProduct().getName());
        txtWishCategory.setText(singleWishItem.getProduct().getCategory().getName());
        txtWishProductPrice.setText("₱ " + singleWishItem.getProduct().getPrice());
        String image_path = Global.STORAGE_PATH + "products/";
        Picasso.get().load(image_path +  singleWishItem.getProduct().getImage()).into(imgWishItemPic);
        btnRemoveFromWish.setOnClickListener(v -> {
            WishlistPresenter mWishListPresenter = new WishlistPresenter((WishlistPresenter.View)this.getContext());
            mWishListPresenter.removeFromWish(singleWishItem);
        });
        btnWishAddToCart.setOnClickListener(v -> {
            WishlistPresenter mWishListPresenter = new WishlistPresenter((WishlistPresenter.View)this.getContext());
            mWishListPresenter.addToCart(singleWishItem);
        });

        //show size
        sprSize.setAdapter(null);
        List<String> spinnerSizeArray =  new ArrayList<String>();
        for (String size : singleWishItem.getProduct().getSizes()) {
            spinnerSizeArray.add(size);
        }

        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<String>(super.getContext(), android.R.layout.simple_spinner_item, spinnerSizeArray);
        sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sprSize.setAdapter(sizeAdapter);
        sprSize.setSelection(0);

        //show colors
        sprColor.setAdapter(null);
        List<String> spinnerColorArray =  new ArrayList<String>();
        for (SubCategory subCategory : singleWishItem.getProduct().getSubCategories()) {
            spinnerColorArray.add(subCategory.getCategory().getName());
        }

        ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(super.getContext(), android.R.layout.simple_spinner_item, spinnerColorArray);
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sprColor.setAdapter(colorAdapter);
        sprColor.setSelection(0);

        return customView;
    }
}
