package com.example.papsirehv.sam_v11.garments_recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemRecyclerViewAdapter.ItemViewHolder> {

    class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView lblProductName, lblCategory, lblPrice;
        private ImageView imgProduct;

        public ItemViewHolder(View itemView) {
            super(itemView);
            lblProductName = itemView.findViewById(R.id.lblProductName);
            lblCategory = itemView.findViewById(R.id.lblCategory);
            lblPrice = itemView.findViewById(R.id.lblPrice);
            imgProduct = itemView.findViewById(R.id.imgProduct);
        }
    }

    private Context context;
    private ArrayList<String> productNames, categories, prices, images;

    public ItemRecyclerViewAdapter(Context context, ArrayList<String> productNames, ArrayList<String> categories, ArrayList<String> prices, ArrayList<String> images) {
        this.context = context;
        this.productNames = productNames;
        this.categories = categories;
        this.prices = prices;
        this.images = images;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_row_layout, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        Picasso.get().load(images.get(position)).into(holder.imgProduct);
        holder.lblProductName.setText(String.valueOf(position + 1) + " " + productNames.get(position));
        holder.lblCategory.setText(categories.get(position));
        holder.lblPrice.setText("₱ " + prices.get(position));
    }

    @Override
    public int getItemCount() {
        return productNames.size();
    }


}
