package com.example.papsirehv.sam_v11.models;

public class OrderDetail {
    private String id;
    private SubCategory sub_category;
    private Product product;
    private String size;
    private int quantity;
    private int sub_total;

    public String getId() {
        return id;
    }

    public SubCategory getSub_category() {
        return sub_category;
    }

    public Product getProduct() {
        return product;
    }

    public String getSize() {
        return size;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getSub_total() {
        return sub_total;
    }
}
