package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class PersonalInfoPresenter {
    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showUserData(User user);
        void clearPasswordField();
    }
    private View view;
    public PersonalInfoPresenter(PersonalInfoPresenter.View view) {
        this.view = view;
    }

    public void showUserData() {
        User user = Global.User;
        view.showUserData(user);
    }
    public void save(User updated_user, DatabaseHandler db) {
        String validated = this.validateUser(updated_user);
        if (!validated.equals("")) {
            view.showErrorAlert(validated);
        } else {
            User user = Global.User;
            user.setFirstName(updated_user.getFirstName());
            user.setMiddleName(updated_user.getMiddleName());
            user.setLastName(updated_user.getLastName());
            user.setEmail(updated_user.getEmail());
            user.setUsername(updated_user.getUsername());
            user.setAddress(updated_user.getAddress());
            user.setPhoneNumber(updated_user.getPhoneNumber());

            String url = Global.API_URL + "user/update";

            user.save(
                    url,
                    (String result) -> {
                        Gson gson = new Gson();
                        try {
                            User loginUser = gson.fromJson(result, User.class);

                            if (loginUser.getId() != null) {
                                Global.User = loginUser;
                                db.deleteAll();
                                db.insertData(loginUser);
                                view.showSuccessAlert("Personal Info has been updated.");

                            } else {
                                view.showErrorAlert("An error occured.");
                            }

                        } catch (Exception ex) {
                            view.showErrorAlert(ex.toString());
                        }
                    },
                    (String error) -> view.showErrorAlert(error)
            );
        }

    }

    public void changePassword(String currentPassword, String newPassword, String confirmPassword) {
        String validate = this.validatePassword(currentPassword, newPassword, confirmPassword);
        if (validate.equals("")) {
            User user = Global.User;
            String url = Global.API_URL + "user/change/password/update";
            user.changePassword(currentPassword, newPassword, url,
                    (String result) -> {
                        Gson gson = new Gson();
                        try {
                            User loginUser = gson.fromJson(result, User.class);

                            if (loginUser.getId() != null) {

                                view.showSuccessAlert("Password has been updated.");
                                view.clearPasswordField();

                            } else {
                                view.showErrorAlert(result);
                            }

                        } catch (Exception ex) {
                            view.showErrorAlert(ex.toString());
                        }
                    },
                    (String error) -> {

                    });
        } else {
            view.showErrorAlert(validate);
        }
    }

    private String validateUser(User user) {
        if (user.getFirstName().equals("")) {
            return "First name field is required.";
        } else if (user.getEmail().equals("")) {
            return "Email field is required.";
        } else if (user.getUsername().equals("")) {
            return "Username field is required.";
        }
        return "";
    }
    private String validatePassword(String currentPassword, String newPassword, String confirmPassword) {
        if (currentPassword.equals("")) {
            return "Current Password is required.";
        } else if (newPassword.equals("")) {
            return "New Password is required.";
        } else if (confirmPassword.equals("")) {
            return "Confirm Password is required.";
        } else if (!confirmPassword.equals(newPassword)) {
            return "Password mismatched.";
        }
        return "";
    }
}
