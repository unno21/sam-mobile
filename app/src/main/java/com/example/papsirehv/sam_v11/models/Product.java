package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;

import java.util.ArrayList;

public class Product {
    private String id;
    private String name;
    private String image;
    private Category category;
    private ArrayList<SubCategory> product_filters;
    private double price;
    private int stock;
    private int rate = 0;
    private String sizes;
    private ArrayList<Review> product_reviews;

    public Product() {}

    public String getId() { return this.id; }
    public String getName() { return this.name; }
    public String getImage() { return this.image; }
    public Category getCategory() { return this.category; }
    public double getPrice() { return this.price; }
    public int getStock() { return this.stock; }
    public int getStarRate() { return this.rate; }
    public ArrayList<Review> getReviews() { return this.product_reviews; }
    public ArrayList<SubCategory> getSubCategories() { return this.product_filters; }
    public String[] getSizes() {
        return this.sizes.split(",");
    }

    public static void getAllFromAPI(String url, Callback successCallback, Callback errorCallback) {
        String requestType = "GET";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }
    public static void getAllFromAPI(String url, Callback successCallback) {
        getAllFromAPI(url, successCallback, (String error) -> {});
    }

}
