package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class SignupCustomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_custom);

        EditText txtCodeFullName = findViewById(R.id.txtCodeFullName);
        EditText txtCodeEmail = findViewById(R.id.txtCodeEmail);
        EditText txtCodePassword = findViewById(R.id.txtCodePassword);
        EditText txtCodeConfirmPassword = findViewById(R.id.txtCodeConfirmPassword);

        Button btnCodeUpdate = findViewById(R.id.btnCodeUpdate);

        btnCodeUpdate.setOnClickListener(v -> {

            String fullname = txtCodeFullName.getText().toString();
            String email = txtCodeEmail.getText().toString();
            String password = txtCodePassword.getText().toString();
            String confirm = txtCodeConfirmPassword.getText().toString();

            boolean hasDataAllField =  checkAllField(fullname, email, password, confirm);

            if (!hasDataAllField) {
                Toast.makeText(SignupCustomActivity.this, "All fields required.", Toast.LENGTH_SHORT).show();
            } else if (!password.equals(confirm)) {
                Toast.makeText(SignupCustomActivity.this, "Password mismatched.", Toast.LENGTH_SHORT).show();
            } else {

                String[] name = (fullname).split(" ");
                User user = Global.User;
                if (name.length > 2) {
                    user.setFirstName(name[0]);
                    user.setMiddleName(name[1]);
                    user.setLastName(name[2]);
                } else if (name.length > 1) {
                    user.setFirstName(name[0]);
                    user.setMiddleName(" ");
                    user.setLastName(name[1]);
                }  else {
                    user.setFirstName(name[0]);
                    user.setMiddleName(" ");
                    user.setLastName(" ");
                }

                btnCodeUpdate.setBackgroundResource(R.drawable.button_bg_disable);
                btnCodeUpdate.setEnabled(false);

                user.setEmail(email);
                user.setPassword(password);

                DatabaseHandler db = new DatabaseHandler(this);
                String url = Global.API_URL + "user/update";

                user.save(
                        url,
                        (String result) -> {
                            Gson gson = new Gson();

                            btnCodeUpdate.setBackgroundResource(R.drawable.button_bg);
                            btnCodeUpdate.setEnabled(true);

                            try {
                                User loginUser = gson.fromJson(result, User.class);

                                if (loginUser.getId() != null) {
                                    Global.User = loginUser;
                                    db.deleteAll();
                                    db.insertData(loginUser);
                                    Toast.makeText(SignupCustomActivity.this, "Information Saved.", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(SignupCustomActivity.this, HomeActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(SignupCustomActivity.this, "An error occured.", Toast.LENGTH_SHORT).show();
                                }

                            } catch (Exception ex) {
                                //view.showErrorAlert(ex.toString());
                            }
                        },
                        (String error) -> {
                            // view.showErrorAlert(error)
                        }
                );
            }
        });

    }

    private boolean checkAllField(String ...fields)
    {
        for (String s : fields) {
            if (s.equals("")) return false;
        }

        return true;
    }
}
