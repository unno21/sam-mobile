package com.example.papsirehv.sam_v11.models;

public class Category {
    private String id;
    private String name;

    public Category() {}

    public String getName() { return this.name; }
    public String getId() { return this.id; }
}
