package com.example.papsirehv.sam_v11.account;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.models.OrderDetail;
import com.example.papsirehv.sam_v11.models.ProductReturn;
import com.example.papsirehv.sam_v11.models.ProductReview;
import com.example.papsirehv.sam_v11.models.Review;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.example.papsirehv.sam_v11.views.GarmentDetailsActivity;
import com.example.papsirehv.sam_v11.views.OrderDetailsActivity;
import com.example.papsirehv.sam_v11.views.PreviousTransactionsActivity;
import com.example.papsirehv.sam_v11.views.ReviewActivity;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderListAdapter extends ArrayAdapter<OrderDetail> {

    private String imagePath;

    public OrderListAdapter(OrderDetailsActivity context, OrderDetail[] orderDetails, String imagePath) {
        super(context, R.layout.custom_order_row, orderDetails);
        this.imagePath = imagePath;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater ordersInflater = LayoutInflater.from(getContext());
        View customView = ordersInflater.inflate(R.layout.custom_order_row, parent, false);

        TextView txtOrderDetailProductName = customView.findViewById(R.id.txtOrderDetailProductName);
        TextView txtOrderDetailQuantity = customView.findViewById(R.id.txtOrderDetailQuantity);
        TextView txtOrderDetailSubTotal = customView.findViewById(R.id.txtOrderDetailSubTotal);
        ImageView imgOrderDetailProductImage = customView.findViewById(R.id.imgOrderDetailProductImage);
        TextView txtWriteReview = customView.findViewById(R.id.btnWriteReview);
        TextView textReturnItem = customView.findViewById(R.id.btnReturnItem);

        OrderDetail orderDetail = this.getItem(position);

        txtOrderDetailProductName.setText(orderDetail.getProduct().getName());
        txtOrderDetailQuantity.setText("x " + String.valueOf(orderDetail.getQuantity()));
        txtOrderDetailSubTotal.setText("₱  " + String.valueOf(orderDetail.getSub_total() + ".00"));

        Picasso.get().load(imagePath + orderDetail.getSub_category().getImage()).into(imgOrderDetailProductImage);

        if (Global.SelectedOrder.getOrder_status().toUpperCase().equals("DELIVERED")) {
            Global.SelectedOrderDetail = orderDetail;
            txtWriteReview.setOnClickListener(v -> {

                this.getContext().startActivity(new Intent(this.getContext(), ReviewActivity.class));
            });
            textReturnItem.setOnClickListener(v -> {
                Dialog dialogReturnItem = new Dialog(this.getContext());

                dialogReturnItem.setContentView(R.layout.dialog_return_item);
                dialogReturnItem.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogReturnItem.show();

                ImageView imgReturnProduct = dialogReturnItem.findViewById(R.id.imgReturnImage);
                TextView lblReturnProductName = dialogReturnItem.findViewById(R.id.lblReturnProductName);
                Spinner sprReturnQuantity = dialogReturnItem.findViewById(R.id.spnrReturnQuantity);
                EditText txtReturnNote = dialogReturnItem.findViewById(R.id.txtReturnNote);
                Button btnReturnSubmit = dialogReturnItem.findViewById(R.id.btnReturnSubmit);

                ArrayList<Button> btnReturnTemplates = new ArrayList<>();
                btnReturnTemplates.add(dialogReturnItem.findViewById(R.id.btnReturnTemplate1));
                btnReturnTemplates.add(dialogReturnItem.findViewById(R.id.btnReturnTemplate2));
                btnReturnTemplates.add(dialogReturnItem.findViewById(R.id.btnReturnTemplate3));
                for (Button b: btnReturnTemplates) {
                    b.setOnClickListener( buttonView -> {
                        Button eventSender = (Button)buttonView;
                        String toAppend = txtReturnNote.getText().toString() + eventSender.getText().toString() + ". ";
                        txtReturnNote.setText(toAppend);
                    });
                }

                Picasso.get().load(Global.STORAGE_PATH + "products/" + orderDetail.getSub_category().getImage()).into(imgReturnProduct);
                lblReturnProductName.setText(orderDetail.getProduct().getName());

                /*
                    QUANTITY SPINNER
                 */
                sprReturnQuantity.setAdapter(null);
                List<String> spinnerqQuantityArray =  new ArrayList<String>();

                for(int i = 1; i <= orderDetail.getQuantity(); i++) {
                    spinnerqQuantityArray.add(String.valueOf(i));
                }

                ArrayAdapter<String> sizeAdapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_spinner_item, spinnerqQuantityArray);
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                sprReturnQuantity.setAdapter(sizeAdapter);
                sprReturnQuantity.setSelection(orderDetail.getQuantity() - 1);

                btnReturnSubmit.setOnClickListener( buttonView -> {
                    btnReturnSubmit.setEnabled(false);
                    btnReturnSubmit.setBackgroundResource(R.drawable.button_bg_disable);
                    //product_id, order_detail_id, buyer_note, quantity
                    ProductReturn productReturn = new ProductReturn();
                    productReturn.setProduct_id(orderDetail.getProduct().getId());
                    productReturn.setOrder_detail_id(orderDetail.getId());
                    productReturn.setBuyer_note(txtReturnNote.getText().toString());
                    productReturn.setQuantity(sprReturnQuantity.getSelectedItemPosition() + 1);

                    String url = Global.API_URL + "order/return";
                    productReturn.save(url,
                            result -> {
                                Toast.makeText(this.getContext(), "Returnn item is on process.", Toast.LENGTH_SHORT).show();
                                dialogReturnItem.dismiss();

                                btnReturnSubmit.setEnabled(true);
                                btnReturnSubmit.setBackgroundResource(R.drawable.button_bg);
                            },
                            error -> {
                                //Toast.makeText(this.getContext(), error, Toast.LENGTH_SHORT).show();

                                Toast.makeText(this.getContext(), "Returnn item is on process.", Toast.LENGTH_SHORT).show();
                                dialogReturnItem.dismiss();

                                btnReturnSubmit.setEnabled(true);
                                btnReturnSubmit.setBackgroundResource(R.drawable.button_bg);
                            }
                    );

                });

            });

        } else {
            txtWriteReview.setVisibility(View.GONE);
            textReturnItem.setVisibility(View.GONE);
        }


//        ImageView imgUserProfilePic = (ImageView) customView.findViewById(R.id.imgUserProfilePic);

        return customView;
    }
}
