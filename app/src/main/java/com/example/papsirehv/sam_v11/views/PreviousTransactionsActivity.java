package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.account.AccountSettingsAdapter;
import com.example.papsirehv.sam_v11.account.OrderListAdapter;
import com.example.papsirehv.sam_v11.account.PreviousTransactionsAdapter;
import com.example.papsirehv.sam_v11.models.Order;
import com.example.papsirehv.sam_v11.presenters.PreviousTransactionsPresenter;

public class PreviousTransactionsActivity extends AppCompatActivity implements PreviousTransactionsPresenter.View {

    private Dialog ordersDialog;
    private PreviousTransactionsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_transactions);

        this.mPresenter = new PreviousTransactionsPresenter(this);
        mPresenter.showOrders();

        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle("Recent Orders");
        mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

        mToolbar.setNavigationIcon(R.drawable.ic_back);
        mToolbar.setNavigationOnClickListener(view -> finish());

        ordersDialog = new Dialog(this);


    }

    @Override
    public void showSuccessAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOrders(Order[] orders) {

        ListAdapter previousTransactionsAdapter = new PreviousTransactionsAdapter(this, orders);
        ListView previousTransactionsListView = findViewById(R.id.lstPreviousTransactions);
        previousTransactionsListView.setAdapter(previousTransactionsAdapter);

        previousTransactionsListView.setOnItemClickListener((AdapterView<?> parent, View view, int position, long id) -> {

            mPresenter.showOrderDetails(position);

        });
    }

    @Override
    public void showOrderDetials() {
        startActivity(new Intent(this, OrderDetailsActivity.class));
    }


}
