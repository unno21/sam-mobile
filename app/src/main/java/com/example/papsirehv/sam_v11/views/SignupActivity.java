package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.presenters.SignupPresenter;

public class SignupActivity extends AppCompatActivity implements SignupPresenter.View {

    Dialog accountVerificationDialog;
    SignupPresenter mSignupPresenter;

    EditText txtFullname, txtUsername, txtPhoneNumber, txtEmail, txtPassword, txtConfirm;
    TextView btnVerify;
    Button btnSignupReg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        /*
            Initialize controls
         */
        txtFullname = findViewById(R.id.txtFullname);
        txtUsername = findViewById(R.id.txtUsername);
        txtPhoneNumber = findViewById(R.id.txtPhoneNumber);
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        txtConfirm = findViewById(R.id.txtConfirm);
        btnSignupReg = findViewById(R.id.btnSignupReg);
        btnVerify = findViewById(R.id.btnVerify);

        accountVerificationDialog = new Dialog(this); // 09/13/18 added by Rehv
        mSignupPresenter = new SignupPresenter(this);

        btnSignupReg.setOnClickListener(v ->{
            String fullname = txtFullname.getText().toString();
            String username = txtUsername.getText().toString();
            String phonenumber = txtPhoneNumber.getText().toString();
            String email = txtEmail.getText().toString();
            String password = txtPassword.getText().toString();
            String confirm = txtConfirm.getText().toString();

            mSignupPresenter.register(fullname, username, phonenumber, email, password, confirm);
        });

        btnVerify.setOnClickListener(v -> {
            this.showVerification();
        });
    }

    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showVerification() {
        accountVerificationDialog.setContentView(R.layout.account_verification);
        accountVerificationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        accountVerificationDialog.show();

        EditText txtVerificationCode = accountVerificationDialog.findViewById(R.id.txtVerificationCode);
        Button btnVerify = accountVerificationDialog.findViewById(R.id.btnVerify);

        btnVerify.setOnClickListener(v -> {
            String code = txtVerificationCode.getText().toString();
            mSignupPresenter.verify(code);
        });
    }

    @Override
    public void showHome() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
    }

    @Override
    public void enableSignUpButton() {
        btnSignupReg.setBackgroundResource(R.drawable.button_bg);
        btnSignupReg.setEnabled(true);
    }

    @Override
    public void disableSignUpButton() {
        btnSignupReg.setBackgroundResource(R.drawable.button_bg_disable);
        btnSignupReg.setEnabled(false);
    }
}
