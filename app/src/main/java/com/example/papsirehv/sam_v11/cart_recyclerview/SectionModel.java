package com.example.papsirehv.sam_v11.cart_recyclerview;

import com.example.papsirehv.sam_v11.models.Item;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class SectionModel {
    private String sectionLabel;
    private ArrayList<Item> items;
    private String image_path;


    public SectionModel(String sectionLabel, ArrayList<Item> items, String image_path) {
        this.sectionLabel = sectionLabel;
        this.items = items;
        this.image_path = image_path;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }
    public String getImagePath() {
        return this.image_path;
    }

}
