package com.example.papsirehv.sam_v11.utilities;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.papsirehv.sam_v11.models.User;

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String DB_NAME = "sam.db";
    public static final String TABLE_NAME = "scores";
    public static final String COL_ID = "id";
    public static final String COL_UNIQUEID = "unique_id";
    public static final String COL_FIRSTNAME = "first_name";
    public static final String COL_MIDDLENAME = "middle_name";
    public static final String COL_LASTNAME = "last_name";
    public static final String COL_EXTENSIONNAME = "extension_name";
    public static final String COL_USERNAME = "username";
    public static final String COL_EMAIL = "email";
    public static final String COL_PASSWORD = "password";
    public static final String COL_ADDRESS = "address";
    public static final String COL_PHONENUMBER = "phone_number";
    public static final String COL_STATUS = "status";
    public static final String COL_TYPE = "type";

    public DatabaseHandler(Context context)
    {
        super(context, DB_NAME, null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + TABLE_NAME +
                " (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "unique_id TEXT, " +
                "first_name TEXT, " +
                "middle_name TEXT, " +
                "last_name TEXT, " +
                "extension_name TEXT, " +
                "username TEXT, " +
                "email TEXT, " +
                "password TEXT, " +
                "address TEXT, " +
                "phone_number TEXT, " +
                "status TEXT, " +
                "type TEXT)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }
    public boolean insertData(User user)
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        ContentValues contentvalue = new ContentValues();
        contentvalue.put(COL_UNIQUEID, user.getId());
        contentvalue.put(COL_FIRSTNAME, user.getFirstName());
        contentvalue.put(COL_MIDDLENAME, user.getMiddleName());
        contentvalue.put(COL_LASTNAME, user.getLastName());
        contentvalue.put(COL_EXTENSIONNAME, user.getExtensionName());
        contentvalue.put(COL_USERNAME, user.getUsername());
        contentvalue.put(COL_EMAIL, user.getEmail());
        //contentvalue.put(COL_PASSWORD, password);
        contentvalue.put(COL_ADDRESS, user.getAddress());
        contentvalue.put(COL_PHONENUMBER, user.getPhoneNumber());
        contentvalue.put(COL_STATUS, String.valueOf(user.getStatus()));
        contentvalue.put(COL_TYPE, user.getStatus());
        long result  = db.insert(TABLE_NAME,null, contentvalue);

        return result != -1;
    }
    public void deleteAll()
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        db.delete(TABLE_NAME, "", new String[0]);
    }
    public User checkUser()
    {
        SQLiteDatabase db  = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from "+TABLE_NAME + " ORDER BY " + COL_ID + " DESC",null);
        if (cursor != null) {

            if (cursor.moveToFirst()) {


                    String id = cursor.getString(1);
                    String first_name = cursor.getString(2);
                    String middle_name = cursor.getString(3);
                    String last_name = cursor.getString(4);
                    String extension_name = cursor.getString(5);
                    String username = cursor.getString(6);
                    String email = cursor.getString(7);
                    //String password = cursor.getString(8);
                    String address = cursor.getString(9);
                    String phone_number = cursor.getString(10);
                    String status = cursor.getString(11);
                    String type = cursor.getString(12);

                    User user = new User();
                    user.setId(id);
                    user.setFirstName(first_name);
                    user.setMiddleName(middle_name);
                    user.setLastName(last_name);
                    user.setExtensionName(extension_name);
                    user.setUsername(username);
                    user.setEmail(email);
                    user.setAddress(address);
                    user.setPhoneNumber(phone_number);
                    user.setStatus(Integer.parseInt(status));
                    user.setType(type);
                    return user;

            }
        }
        return null;
    }
}
