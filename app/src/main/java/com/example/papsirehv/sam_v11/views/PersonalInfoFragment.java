package com.example.papsirehv.sam_v11.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.presenters.PersonalInfoPresenter;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;

public class PersonalInfoFragment extends Fragment implements PersonalInfoPresenter.View {

    private EditText txtLastName, txtFirstName, txtMiddleName, txtEmail, txtUsername, txtCurrentPassword, txtNewPassword, txtConfirmPassword;
    private EditText txtAddress, txtPhone_Number;
    private Button btnSaveInfo, btnSavePassword;
    private PersonalInfoPresenter mPersonalInfoPresenter;
    private DatabaseHandler db;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        //return inflater.inflate(R.layout.fragment_personal_info,container,false);

        View rootView = inflater.inflate(R.layout.fragment_personal_info, container, false);
        this.txtLastName = rootView.findViewById(R.id.txtLastName);
        this.txtFirstName = rootView.findViewById(R.id.txtFirstName);
        this.txtMiddleName = rootView.findViewById(R.id.txtMiddleName);
        this.txtEmail = rootView.findViewById(R.id.txtEmail);
        this.txtUsername = rootView.findViewById(R.id.txtUsername);
        this.txtAddress = rootView.findViewById(R.id.txtAddress);
        this.txtPhone_Number = rootView.findViewById(R.id.txtPhone_Number);

        this.txtCurrentPassword = rootView.findViewById(R.id.txtCurrentPassword);
        this.txtNewPassword = rootView.findViewById(R.id.txtNewPassword);
        this.txtConfirmPassword = rootView.findViewById(R.id.txtConfirmPassword);
        this.btnSaveInfo = rootView.findViewById(R.id.btnSaveInfo);
        this.btnSavePassword = rootView.findViewById(R.id.btnSavePassword);

        db = new DatabaseHandler(this.getContext());

        this.btnSaveInfo.setOnClickListener(v -> {
            String first_name = this.txtFirstName.getText().toString();
            String middle_name = this.txtMiddleName.getText().toString();
            String last_name = this.txtLastName.getText().toString();
            String email = this.txtEmail.getText().toString();
            String username = this.txtUsername.getText().toString();
            String address = this.txtAddress.getText().toString();
            String phoneNumber = this.txtPhone_Number.getText().toString();

            User user = new User();
            user.setFirstName(first_name);
            user.setMiddleName(middle_name);
            user.setLastName(last_name);
            user.setEmail(email);
            user.setUsername(username);
            user.setAddress(address);
            user.setPhoneNumber(phoneNumber);
            mPersonalInfoPresenter.save(user, db);
        });

        this.btnSavePassword.setOnClickListener(v -> {
            String current_password = this.txtCurrentPassword.getText().toString();
            String new_password = this.txtNewPassword.getText().toString();
            String confirm_password = this.txtConfirmPassword.getText().toString();

            mPersonalInfoPresenter.changePassword(current_password, new_password, confirm_password);
        });

        this.mPersonalInfoPresenter = new PersonalInfoPresenter(this);
        this.mPersonalInfoPresenter.showUserData();

        return rootView;
    }
    @Override
    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showUserData(User user) {
        this.txtLastName.setText(user.getLastName());
        this.txtFirstName.setText(user.getFirstName());
        this.txtMiddleName.setText(user.getMiddleName());
        this.txtEmail.setText(user.getEmail());
        this.txtUsername.setText(user.getUsername());
        this.txtAddress.setText(user.getAddress());
        this.txtPhone_Number.setText(user.getPhoneNumber());
    }

    @Override
    public void clearPasswordField() {
        this.txtCurrentPassword.setText("");
        this.txtNewPassword.setText("");
        this.txtConfirmPassword.setText("");
    }
}
