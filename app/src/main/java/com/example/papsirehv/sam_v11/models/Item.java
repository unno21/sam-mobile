package com.example.papsirehv.sam_v11.models;

import com.example.papsirehv.sam_v11.interfaces.Callback;
import com.example.papsirehv.sam_v11.utilities.AsyncAPI;
import com.example.papsirehv.sam_v11.utilities.KeyValuePair;

import java.util.ArrayList;

public class Item {
    private String id;
    private String cart_id;
    private String size;
    private String color;
    private Product product;
    private int quantity;
    private String sub_category_id;
    private String size_id;

    //from api
    public SubCategory product_filters;

    private SubCategory subCategory;

    public Item(SubCategory subCategory, int quantity) {
        this.subCategory = subCategory;
        this.quantity = quantity;
    }
    public Item() {}
    public Item(SubCategory subCategory) { this.subCategory = subCategory; }
    public Item(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }
    public String getId(){ return this.id; }
    public Product getProduct() { return this.product; }
    public void setQuantity(int quantity) { this.quantity = quantity; }
    public int getQuantity() { return this.quantity; }
    public void setSize(String size) { this.size = size; }
    public String getSize() { return this.size; }
    public void setColor(String color) { this.color = color; this.sub_category_id = color; }
    public String getColor() { return this.color; }

    public String getSub_category_id() { return this.sub_category_id; }
    public String getSize_id() { return this.size_id; }

    public SubCategory getSubcategory() { return this.product_filters == null ? this.subCategory : this.product_filters; }

    private void execute(String url, Callback successCallback, Callback errorCallback) {
        /*
            Prepare data to send to API
         */
        ArrayList<KeyValuePair> data = new ArrayList<>();
        data.add(new KeyValuePair("id", this.id));
        data.add(new KeyValuePair("item_id", this.id));
        data.add(new KeyValuePair("cart_id", this.cart_id));
        data.add(new KeyValuePair("sub_category_id", this.sub_category_id));
//        if (this.product.getSubCategories().size() > 0) {
//            data.add(new KeyValuePair("sub_category_id", this.subCategory.getID()));
//        } else {
//            data.add(new KeyValuePair("sub_category_id", this.color));
//        }

        data.add(new KeyValuePair("size", this.size));
        data.add(new KeyValuePair("quantity", this.quantity));

        String requestType = "POST";

        AsyncAPI asyncAPI = new AsyncAPI()
                .url(url)
                .type(requestType)
                .data(data)
                .success((String response) -> successCallback.Invoke(response))
                .error((String error) -> errorCallback.Invoke(error));

        asyncAPI.execute();
    }

    public void save(String url, Callback successCallback) {
        this.execute(url, successCallback, (String error) -> {});
    }
    public void save(String url, Callback successCallback, Callback errorCallback){
        this.execute(url, successCallback, errorCallback);
    }


}
