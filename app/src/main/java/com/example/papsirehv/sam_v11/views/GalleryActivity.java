package com.example.papsirehv.sam_v11.views;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.papsirehv.sam_v11.BaseActivity;
import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.gallery.DetailActivity;
import com.example.papsirehv.sam_v11.gallery.GalleryAdapter;
import com.example.papsirehv.sam_v11.gallery.ImageModel;
import com.example.papsirehv.sam_v11.gallery.RecyclerItemClickListener;
import com.example.papsirehv.sam_v11.models.Gallery;
import com.example.papsirehv.sam_v11.utilities.Global;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Random;
import com.google.gson.Gson;


public class GalleryActivity extends BaseActivity {

    GalleryAdapter mAdapter;
    RecyclerView mRecyclerView;

    ArrayList<ImageModel> data = new ArrayList<>();

    @Override
    public int getContentViewId() {
        return R.layout.activity_gallery;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_gallery;
    }

    @Override
    public void childOnCreate() {

        String url = Global.API_URL + "gallery/" + Global.User.getId();

        Gallery.getAll(
            url,
            (result) -> {
                Gson gson = new Gson();
                try {
                    Gallery[] galleries = gson.fromJson(result, Gallery[].class);

                    for (Gallery g : galleries) {
                        ImageModel imageModel = new ImageModel();
                        imageModel.setName(g.getId());
                        imageModel.setUrl(Global.STORAGE_PATH + "gallery/" + g.getImage());
                        imageModel.setId(g.getId());
                        data.add(imageModel);
                    }

                    Toolbar mToolbar = findViewById(R.id.toolbar);
                    mToolbar.setTitle("Gallery");
                    mToolbar.setTitleTextColor(Color.parseColor("#ffffff"));

                    RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list);
                    mRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                    mRecyclerView.setHasFixedSize(true);


                    mAdapter = new GalleryAdapter(GalleryActivity.this, data);
                    mRecyclerView.setAdapter(mAdapter);

                    mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                            new RecyclerItemClickListener.OnItemClickListener() {

                                @Override
                                public void onItemClick(View view, int position) {

                                    Intent intent = new Intent(GalleryActivity.this, DetailActivity.class);
                                    intent.putParcelableArrayListExtra("data", data);
                                    intent.putExtra("pos", position);
                                    startActivity(intent);

                                }
                            }));
                } catch (Exception ex) {
                    //view.showErrorAlert(result + "\n" + ex.toString());
                }

            },
            (error) -> {

            }
        );



    }


//    @Override
//    public void onResume(){
//        super.onResume();
//        // put your code here...
//        this.childOnCreate();
//
//    }
}
