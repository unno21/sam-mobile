package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Size;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

public class LoginPresenter {
    public interface View {
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
        void showVerification();
        void showQrPassword(User user);
        void showHome();
        void showSignUpCustom();
        void enableLoginButton();
        void disableLoginButton();
    }

    private View view;
    public LoginPresenter(View view) {
        this.view = view;
    }

    public void login(String code)
    {
        User user = new User();
        String url = Global.API_URL + "user/login/" + code;
        this.loginCodeUrl(url);
    }

    public void loginCodeUrl(String url)
    {
        User user = new User();
        user.loginByCode(url,
                (result) -> {
                    Gson gson = new Gson();
                    try {
                        User loginUser = gson.fromJson(result, User.class);

                        if (loginUser.getId() != null) {
                            Global.User = loginUser;

                            if (loginUser.getFirstName().toUpperCase().equals("NEW") &&
                                    loginUser.getLastName().toUpperCase().equals("CUSTOMER") &&
                                    loginUser.getEmail().contains("sample")) {

                                view.showSuccessAlert("Please provide some of your informations.");
                                view.showSignUpCustom();

                            } else {
                                view.showQrPassword(loginUser);
                            }


                        } else {
                            view.showErrorAlert("Invalid QR Code.");
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                });
    }

    public void login(String email, String password, DatabaseHandler db) {
        User user = new User(email, password);
        String url = Global.API_URL + "user/login";
        view.disableLoginButton();
        user.login(url,
                (String result) -> {
                    view.enableLoginButton();
                    Gson gson = new Gson();
                    try {
                        User loginUser = gson.fromJson(result, User.class);

                        if (loginUser.getId() != null) {
                            Global.User = loginUser;

                            if (loginUser.getStatus() == 0) {
                                view.showErrorAlert("Account not yet verified.");
                                view.showVerification();
                            } else if (!loginUser.getType().equals("user")) {
                                view.showErrorAlert("You are not allowed to log in.");
                            } else {
                                view.showSuccessAlert("Login Success");
                                db.insertData(loginUser);
                                this.checkCart();
                                view.showHome();
                            }

                        } else {
                            view.showErrorAlert("Incorrect password");
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                        view.enableLoginButton();
                    }

                },
                (String error) -> view.showErrorAlert(error)
        );
    }
    public void verify(String code)
    {
        User user = Global.User;
        user.setRandomCode(code);
        String url = Global.API_URL + "user/verify";

        user.verify(url,
                (String result) -> {

                    Gson gson = new Gson();
                    try {
                        User loginUser = gson.fromJson(result, User.class);

                        if (loginUser.getId() != null) {
                            Global.User = loginUser;

                            if (loginUser.getStatus() == 0) {
                                view.showErrorAlert("Invalid verification code.");
                            } else {
                                view.showSuccessAlert("Account has been verified.");
                                view.showHome();
                            }

                        } else {
                            view.showErrorAlert("Incorrect email/password");
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }

                },
                (String error) -> view.showErrorAlert(error)
        );

    }

    public void resetPassword(String email) {
        String url = Global.API_URL + "user/reset/" + email;
        User.resetPassword(
                url,
                (String result) -> {
                    view.showSuccessAlert("Reset password link has been sent to your email.\n" +
                                            "Please check your email.");
                },
                (String error) -> {
                    view.showErrorAlert(error);
                });
    }

    private void checkCart() {

        String url = Global.API_URL + "cart/" + Global.User.getId();
        Cart.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Cart cart = gson.fromJson(result, Cart.class);

                        // create copy for presenter
                        //this.cart = cart;
                        Global.Cart = cart;


                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                        this.checkCart();
                    }
                },
                (String error) -> view.showErrorAlert("")
        );

    }

    public void resendCode()
    {
        User user = Global.User;
        String url = Global.API_URL + "user/resend-code/" + user.getId();

        user.resendCode(url,
                (String result) -> {

                    view.showSuccessAlert(result);

                },
                (String error) -> view.showErrorAlert(error)
        );
    }

    public void checkUser(DatabaseHandler db) {
        User user = db.checkUser();
        if (user != null) {
            Global.User = user;
            checkCart();
            view.showHome();
        }
    }
}
