package com.example.papsirehv.sam_v11.garment_details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.Review;
import com.example.papsirehv.sam_v11.views.GarmentDetailsActivity;

public class ReviewListAdapter extends ArrayAdapter<Review> {

    public ReviewListAdapter(GarmentDetailsActivity context, Review[] reviews) {
        super(context, R.layout.custom_review_row, reviews);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater reviewInflater = LayoutInflater.from(getContext());
        View customView = reviewInflater.inflate(R.layout.custom_review_row, parent, false);

        Review singleReviewItem = getItem(position);
        TextView txtFullname = (TextView) customView.findViewById(R.id.txtFullname);
        TextView txtReview = customView.findViewById(R.id.txtReview);
        RatingBar userRatingBar = customView.findViewById(R.id.userRatingBar);

//        ImageView imgUserProfilePic = (ImageView) customView.findViewById(R.id.imgUserProfilePic);

        txtFullname.setText(singleReviewItem.getUser().getFullName());
        txtReview.setText(singleReviewItem.getComment());
        userRatingBar.setRating(singleReviewItem.getRate());
//        imgUserProfilePic.setImageResource(R.drawable.img_sample_garment);
        return customView;
    }
}
