package com.example.papsirehv.sam_v11.account;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.views.AccountActivity;

public class AccountSettingsAdapter extends ArrayAdapter<String> {

    public AccountSettingsAdapter(AccountActivity context, String[] garments) {
        super(context, R.layout.custom_account_row, garments);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater cartInflater = LayoutInflater.from(getContext());
        View customView = cartInflater.inflate(R.layout.custom_account_row, parent, false);

        String singleAccountSettingItem = getItem(position);
        TextView txtAccountSetting = (TextView) customView.findViewById(R.id.txtAccountSetting);
//        ImageView imgWishItemPic = (ImageView) customView.findViewById(R.id.imgWishItemPic);
//
        txtAccountSetting.setText(singleAccountSettingItem);
//        imgWishItemPic.setImageResource(R.drawable.img_sample_garment);
        return customView;
    }
}
