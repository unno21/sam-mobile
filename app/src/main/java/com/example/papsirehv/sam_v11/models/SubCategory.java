package com.example.papsirehv.sam_v11.models;

public class SubCategory {
    private String id, image, barcode, product_id, category_id;
    private int stock, used_stock;
    private Category category;

    public SubCategory() { }

    public String getID() { return this.id; }
    public String getImage() { return this.image; }
    public String getBarcode() { return this.barcode; }
    public String getProductID() { return this.product_id; }
    public Category getCategory() { return this.category; }
    public int getStock() { return this.stock; }
    public int getUsedStock() { return this.used_stock; }
}
