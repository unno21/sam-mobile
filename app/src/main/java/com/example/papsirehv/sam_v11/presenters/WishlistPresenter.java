package com.example.papsirehv.sam_v11.presenters;

import com.example.papsirehv.sam_v11.models.APIResponse;
import com.example.papsirehv.sam_v11.models.Cart;
import com.example.papsirehv.sam_v11.models.Item;
import com.example.papsirehv.sam_v11.models.Product;
import com.example.papsirehv.sam_v11.models.Wishlist;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;

public class WishlistPresenter {
    public interface View {
        void showWishlist(Wishlist[] wishlist, String image_path);
        void showSuccessAlert(String message);
        void showErrorAlert(String message);
    }
    private WishlistPresenter.View view;
    private Wishlist[] wishlist;
    private Cart cart;
    public WishlistPresenter(WishlistPresenter.View view){ this.view = view; }

    public void showWishlist() {
        String url = Global.API_URL + "wishlist/" + Global.User.getId();
        Wishlist.getFromAPI(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        Wishlist[] wishlist = gson.fromJson(result, Wishlist[].class);

                        // create copy for presenter
                        this.wishlist = wishlist;
                        String image_path = Global.STORAGE_PATH + "products/";
                        view.showWishlist(this.wishlist, image_path);
                    } catch (Exception ex) {
                        //view.showErrorAlert(result + "\n" + ex.toString());
                    }
                },
                (String error) ->{ //view.showErrorAlert(error);
                }
        );
    }
    public void removeFromWish(Wishlist wishlist) {

        String url = Global.API_URL + "wishlist/remove/" + wishlist.getId();
        Wishlist.removeItem(
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                        APIResponse response = gson.fromJson(result, APIResponse.class);

                        if (response.getResult().equals("success")) {
                            view.showSuccessAlert("Item has been removed from your wishlist.");
                            this.showWishlist();
                        } else {
                            view.showErrorAlert(response.getMessage());
                        }

                    } catch (Exception ex) {
                        //view.showErrorAlert(ex.toString());
                    }
                },
                (String error) -> {

                });

    }
    public void addToCart(Wishlist wishlist) {
        this.cart = Global.Cart;
        Item item = new Item(wishlist.getProduct(), 1);
        checkCart();
        String url = Global.API_URL + "cart/add";
        cart.addItem(
                item,
                url,
                (String result) -> {
                    Gson gson = new Gson();
                    try {
                            //view.showErrorAlert(result);
                        Item addItem = gson.fromJson(result, Item.class);

                        if (addItem.getId() != null) {
                            view.showSuccessAlert("Item has been added to your cart.");
                            this.removeFromWish(wishlist);
                        } else {
                            view.showErrorAlert(result);
                        }

                    } catch (Exception ex) {
                        view.showErrorAlert("Not enough stock.");
                    }
                },
                (String error) -> {
                    //view.showErrorAlert(error);
                });
    }
    private void checkCart() {

        if (Global.Cart == null) {
            String url = Global.API_URL + "cart/" + Global.User.getId();
            Cart.getFromAPI(
                    url,
                    (String result) -> {
                        Gson gson = new Gson();
                        try {
                            Cart cart = gson.fromJson(result, Cart.class);

                            // create copy for presenter
                            this.cart = cart;
                            Global.Cart = cart;


                        } catch (Exception ex) {
                            //view.showErrorAlert(result + "\n" + ex.toString());
                            //this.checkCart();
                        }
                    },
                    (String error) -> view.showErrorAlert("")
            );
        }


    }
}
