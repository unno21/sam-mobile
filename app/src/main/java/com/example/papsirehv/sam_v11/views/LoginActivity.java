package com.example.papsirehv.sam_v11.views;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

//import es.dmoral.toasty.Toasty;

import com.example.papsirehv.sam_v11.R;
import com.example.papsirehv.sam_v11.models.User;
import com.example.papsirehv.sam_v11.presenters.LoginPresenter;
import com.example.papsirehv.sam_v11.utilities.DatabaseHandler;
import com.example.papsirehv.sam_v11.utilities.Global;
import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    Dialog forgotPasswordDialog;
    Dialog accountVerificationDialog;
    Dialog loginCodeDialog;
    Dialog qrPasswordDialog;
    LoginPresenter mLonginPresenter;
    DatabaseHandler db;
    Button btnLogin;

    EditText txtEmail, txtPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*
            Initialize controls
         */
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);

        forgotPasswordDialog = new Dialog(this);
        accountVerificationDialog = new Dialog(this);
        loginCodeDialog = new Dialog(this);
        qrPasswordDialog = new Dialog(this);
        mLonginPresenter = new LoginPresenter(this);
        db = new DatabaseHandler(this);
        mLonginPresenter.checkUser(db);
    }

    public void LoginClick(View view){
        String email = txtEmail.getText().toString();
        String password = txtPassword.getText().toString();
        mLonginPresenter.login(email, password, db);
    }

    public void SignupClick(View view){
        Intent i = new Intent(this, SignupActivity.class);
        startActivity(i);
    }

    public void ForgotPasswordClick(View view) {
        forgotPasswordDialog.setContentView(R.layout.custom_forgot_password);
        forgotPasswordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        forgotPasswordDialog.show();

        EditText txtEmailAddress = forgotPasswordDialog.findViewById(R.id.txtEmailAddress);
        EditText txtPhoneNumber = forgotPasswordDialog.findViewById(R.id.txtPhoneNumber);
        Button btnSubmit = forgotPasswordDialog.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(v -> {
            String email = txtEmailAddress.getText().toString();
            mLonginPresenter.resetPassword(email);
        });
    }

    public void LoginCodeClick(View view){
//        loginCodeDialog.setContentView(R.layout.custom_login_code);
//        loginCodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        loginCodeDialog.show();
//
//        EditText txtCode = loginCodeDialog.findViewById(R.id.txtLoginCode);
//        Button btnSignIn = loginCodeDialog.findViewById(R.id.btnSignIn);
//        btnSignIn.setOnClickListener(v -> {
//            String code = txtCode.getText().toString();
//            mLonginPresenter.login(code);
//        });

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();

            } else {
                //Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                String qr_code =  result.getContents();
                mLonginPresenter.loginCodeUrl(qr_code);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    @Override
    public void showSuccessAlert(String message) {
        //Toasty.success(this,message, Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorAlert(String message) {
        //Toasty.error(this,message,Toast.LENGTH_SHORT).show();
        Toast.makeText(this.getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showVerification() {
        accountVerificationDialog.setContentView(R.layout.account_verification);
        accountVerificationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        accountVerificationDialog.show();

        EditText txtVerificationCode = accountVerificationDialog.findViewById(R.id.txtVerificationCode);
        Button btnVerify = accountVerificationDialog.findViewById(R.id.btnVerify);
        Button btnResend = accountVerificationDialog.findViewById(R.id.btnResend);

        btnVerify.setOnClickListener(v -> {
            String code = txtVerificationCode.getText().toString();
            mLonginPresenter.verify(code);
        });

        btnResend.setOnClickListener(v -> {
            mLonginPresenter.resendCode();
        });
    }

    @Override
    public void showQrPassword(User user) {
        qrPasswordDialog.setContentView(R.layout.qr_account_password);
        qrPasswordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        qrPasswordDialog.show();

        TextView lblGreetings = qrPasswordDialog.findViewById(R.id.txtGreetings);
        EditText txtQrPassword = qrPasswordDialog.findViewById(R.id.txtQrPassword);
        Button btnQrLoginWithPassword = qrPasswordDialog.findViewById(R.id.btnQrLoginWithPassword);

        lblGreetings.setText("Hello " + user.getFirstName() + ",");

        btnQrLoginWithPassword.setOnClickListener(v -> {
            String password = txtQrPassword.getText().toString();
            mLonginPresenter.login(user.getEmail(), password, db);
        });
    }

    @Override
    public void showHome() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void showSignUpCustom() {
        Intent i = new Intent(this, SignupCustomActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void enableLoginButton() {
        btnLogin.setBackgroundResource(R.drawable.button_bg);
        btnLogin.setEnabled(true);
    }

    @Override
    public void disableLoginButton() {
        btnLogin.setBackgroundResource(R.drawable.button_bg_disable);
        btnLogin.setEnabled(false);
    }
}
