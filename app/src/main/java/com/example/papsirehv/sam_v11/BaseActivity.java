package com.example.papsirehv.sam_v11;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.papsirehv.sam_v11.views.AccountActivity;
import com.example.papsirehv.sam_v11.views.CartActivity;
import com.example.papsirehv.sam_v11.views.GalleryActivity;
import com.example.papsirehv.sam_v11.views.HomeActivity;
import com.example.papsirehv.sam_v11.views.WishListActivity;

public abstract class BaseActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    protected BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentViewId());
        childOnCreate();

        navigationView = (BottomNavigationView) findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateNavigationBarState();
    }

    // Remove inter-activity transition to avoid screen tossing on tapping bottom navigation items
    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        navigationView.postDelayed(()   -> {
            int itemId = item.getItemId();
            switch (itemId) {
                case R.id.navigation_home:
                    startActivity(new Intent(this, HomeActivity.class));
                    break;
                case R.id.navigation_wishlist:
                    startActivity(new Intent(this, WishListActivity.class));
                    break;
                case R.id.navigation_gallery:
                    startActivity(new Intent(this, GalleryActivity.class));
                    break;
                case R.id.navigation_cart:
                    startActivity(new Intent(this, CartActivity.class));
                    break;
                case R.id.navigation_account:
                    startActivity(new Intent(this, AccountActivity.class));
                    break;
            }

            finish();
        },300);
        return true;
    }

    private void updateNavigationBarState(){
        int actionId = getNavigationMenuItemId();
        selectBottomNavigationBarItem(actionId);
    }

    void selectBottomNavigationBarItem(int itemId) {
        MenuItem item = navigationView.getMenu().findItem(itemId);
        item.setChecked(true);
    }

    public abstract int getContentViewId();

    public abstract int getNavigationMenuItemId();

    public abstract void childOnCreate();

}
